using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PotionEffect : MonoBehaviour
{
    [SerializeField] public UnityEvent Buff;

    private ThirdPersonController player;
    private Animator animator;
    [SerializeField] private UI_lean_tween uiSystem;

    [SerializeField] private Transform potionPlace;
    [SerializeField] private GameObject potionObject;
    [SerializeField] private GameObject potionHand;
    [SerializeField] private float staminaUnstoppableTime;
    [SerializeField] private float heartsMultTime;
    [SerializeField] private float heartsMultiplier;
    [SerializeField] private float berserkTime;
    [SerializeField] private float BerserkMultiplier;
    [SerializeField] private int numOfPotions;

    

    private void Awake()
    {
        player = ThirdPersonController.instance;
        uiSystem = player.ui_stats;

        
    }

    public void TakeBuff()
    {
        Debug.Log("TookBuff");
        Buff.Invoke();
    }

    public void StartTakinngBuff()
    {
        if (!animator)
            animator = ThirdPersonController.instance._animator;
        Debug.Log("Playing anim");
        animator.SetTrigger(player._animIDPotion);
    }

    public void HealPotion()
    {
        uiSystem.Add_hp(player.playerStats.max_health / 4); // Adding 25% of maximum health to player
        player.playerStats.health = Mathf.Clamp(player.playerStats.health, 0, player.playerStats.max_health);
    }

    public void Stamina()
    {
        player.StaminaUnStopable(staminaUnstoppableTime);
    }

    public void Hearts()
    {
        player.HeartMultiplier(heartsMultiplier);
    }
    
    public void Berserk()
    {
        player.BecomeBerserk(BerserkMultiplier, berserkTime);
    }
    
}
