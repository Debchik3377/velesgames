using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CinemachineController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera lockonCam;
    [SerializeField] private CinemachineVirtualCamera simpleCam;
    [SerializeField] private CinemachineVirtualCamera[] cams;

    [SerializeField] float maxDistanceToPlayer;

    private float shakeTimer;
    private float shakeTimerTotal;
    private float startingIntencity;

    public void SetLockTarget(Transform lockOnPosition)
    {
        lockonCam.LookAt = lockOnPosition;
    }

    public void TurnOnLockon()
    {
        lockonCam.Priority = 10;
        simpleCam.Priority = 5;
    }

    public void TurnOffLockon() 
    {
        lockonCam.Priority = 5;
        simpleCam.Priority = 10;
    }

    public void ShakeCam(float intensity, float time)
    {
        CinemachineBasicMultiChannelPerlin[] cinemachinePerlines = new CinemachineBasicMultiChannelPerlin[cams.Length];
        for(int i = 0; i < cams.Length; ++i)
        {
            cinemachinePerlines[i] = cams[i].GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
            cinemachinePerlines[i].m_AmplitudeGain = intensity;

            startingIntencity = intensity;
            shakeTimerTotal = time;
            shakeTimer = time;
        }
    }

    private void Update()
    {

        if (shakeTimer > 0f)
        {
            shakeTimer -= Time.deltaTime;

            CinemachineBasicMultiChannelPerlin[] cinemachinePerlines = new CinemachineBasicMultiChannelPerlin[cams.Length];
            for (int i = 0; i < cams.Length; ++i)
            {
                cinemachinePerlines[i] = cams[i].GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

                cinemachinePerlines[i].m_AmplitudeGain = Mathf.Lerp(
                    startingIntencity, 0f, (1 - (shakeTimer / shakeTimerTotal))
                    );
            }
        }
    }
}
