using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private Transform targetTransform;
    [SerializeField] LayerMask obstacleLayerMask;
    private Transform _transform;


    private void Start()
    {
        _transform = GetComponent<Transform>();
    }

    private void FixedUpdate()
    {
        if (Physics.Linecast(_transform.position, targetTransform.position, obstacleLayerMask))
        {
            Debug.Log("There is an obstacle");
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(_transform.position, targetTransform.position);
    }
}
