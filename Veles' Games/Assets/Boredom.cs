using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boredom : StateMachineBehaviour
{
    [SerializeField]
    private float _timeUntilBored;
    
    [SerializeField]
    private float _timeUntilMegaBored;

    [SerializeField]
    private int _numberOfBoredAnimations;

    [SerializeField]
    private int _megaBoredAnimation;

    private bool _isBored;
    private bool _isMegaBored;
    private float _idleTime;
    private float _idleLongTime;
    private int _boredAnimation;

    private int _boredLoop;

    private static int _animIDBored;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _boredLoop = 0;
        _animIDBored = Animator.StringToHash("Bored");
        ResetBoredIdle();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_isBored == false && _isMegaBored == false)
        {
            _idleTime += Time.deltaTime;
            _idleLongTime += Time.deltaTime;

            if (_idleLongTime > _timeUntilMegaBored && stateInfo.normalizedTime % 1 < 0.02f)
            {
                _isMegaBored = true;

                _boredAnimation = _megaBoredAnimation;

                animator.SetFloat(_animIDBored, _boredAnimation - 1);
            }
            else if (_idleTime > _timeUntilBored && stateInfo.normalizedTime % 1 < 0.02f)
            {
                _isBored = true;
                _boredAnimation = Random.Range(1, _numberOfBoredAnimations + 1);
                _boredAnimation = _boredAnimation * 2 - 1;

                animator.SetFloat(_animIDBored, _boredAnimation - 1);
            }

            
        }
        else if (stateInfo.normalizedTime % 1 > 0.98f)
        {
            if (_isBored && !_isMegaBored)
            {
                ResetBoredIdle();
            }
            if (_isMegaBored)
            {
                if (_boredLoop > 5)
                {
                    ResetMegaBoredIdle();
                }
                else
                {
                    _boredLoop += 1;
                }
            }

        }

        animator.SetFloat(_animIDBored, _boredAnimation, 0.2f, Time.deltaTime); 
    }

    private void ResetBoredIdle()
    {
        if (_isBored)
        {
            _boredAnimation--;
        }

        _isBored = false;
        _idleTime = 0;
    }

    private void ResetMegaBoredIdle()
    {
        if (_isMegaBored)
        {
            _boredAnimation--;
        }
        _isBored = false;
        _isMegaBored = false;
        _idleTime = 0;
        _idleLongTime = 0;
        _boredLoop = 0;
    }
}
