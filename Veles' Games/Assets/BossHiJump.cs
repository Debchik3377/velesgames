using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BossHiJump : StateMachineBehaviour
{
    NavMeshAgent agent;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        BossMono.instance.StopMoving();
        agent = animator.GetComponent<NavMeshAgent>();

        animator.applyRootMotion = true;
        //BossMono.instance.ChangeMovingForward(true);
        agent.updatePosition = false;
        agent.updateRotation = false;
        agent.velocity = Vector3.zero;
        //BossMono.instance.GoTowardsPlayer();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //BossMono.instance.StopMoving();
        animator.applyRootMotion = false;
        //BossMono.instance.ChangeMovingForward(false);
        agent.updatePosition = true;
        agent.updateRotation = true;
        agent.isStopped = true;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //agent.Move(animator.deltaPosition);
        /*agent.nextPosition = animator.transform.position;
        animator.transform.position = animator.rootPosition;*/
    }

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
