﻿Shader "NatureManufacture Shaders/Water/Water Particles Foam"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,0)
		_ColorPower("Color Power", Vector) = (5,5,5,0)
		_AOPower("AO Power", Range( 0 , 1)) = 1
		_Opacity("Opacity", Range( 0 , 20)) = 0.23
		_ParticleTexture("Particle Texture", 2D) = "white" {}
		_ParticleNormalmap("Particle Normalmap", 2D) = "bump" {}
		_NormalScale("Normal Scale", Range( -5 , 5)) = 0
		_Smoothness("Smoothness", Range( 0 , 1)) = 0.8
		_Metallic("Metallic", Range( 0 , 1)) = 0.1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" }
		Cull Off
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#pragma target 4.5

		//						↓	 			 					↓↓↓↓↓↓↓ AURA 2 COPY THIS TOO ↓↓↓↓↓↓↓ (ATTENTION : Please verify the shading model used by your shader at the begining of the line here under. Use metaPass_Standard If your shading model is "Standard", or metaPass_StandardSpecular if your shading model is "StandardSpecular")
		#pragma surface surf Standard alpha:fade keepalpha noshadow finalcolor:metaPass_Standard
		//						↑									↑↑↑↑↑↑↑ AURA 2 COPY THIS TOO ↑↑↑↑↑↑↑ (ATTENTION : Please verify the shading model used by your shader at the begining of the line here over. Use metaPass_Standard If your shading model is "Standard", or metaPass_StandardSpecular if your shading model is "StandardSpecular")

		struct Input
		{
			float2 uv_texcoord;
			float4 vertexColor : COLOR;
			float4 screenPos; // Needed for Aura 2
		};

//↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
//	AURA 2 : COPY THIS BLOCK AFTER THE Input STRUCT	(ATTENTION THE Input STRUCT NEEDS "screenPos")	↓↓
//↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
		///-------------------------------------------------------------------------------------------
		///			Aura 2 variants
		///-------------------------------------------------------------------------------------------
		#pragma multi_compile _ AURA
		#pragma multi_compile _ AURA_USE_DITHERING
		#pragma multi_compile _ AURA_USE_CUBIC_FILTERING
		#pragma multi_compile _ AURA_DISPLAY_VOLUMETRIC_LIGHTING_ONLY
		///-------------------------------------------------------------------------------------------
		///			Aura 2 surface shaders specific include
		///-------------------------------------------------------------------------------------------
		#if defined(AURA)
			#include "Assets/Aura 2/Core/Code/Shaders/Includes/AuraSurface.cginc"
		#endif // AURA
	
		///-------------------------------------------------------------------------------------------
		///			Apply Aura 2 after surface shader has been rendered
		///-------------------------------------------------------------------------------------------
		void metaPass_Standard(Input IN, SurfaceOutputStandard o, inout fixed4 color)
		{
			#if defined(AURA)
			Aura2_MetaPass_Standard(IN, o, color); // ATTENTION Input struct needs "screenPos"
			#endif // AURA
		}
		
		void metaPass_StandardSpecular(Input IN, SurfaceOutputStandardSpecular o, inout fixed4 color)
		{
			#if defined(AURA)
			Aura2_MetaPass_StandardSpecular(IN, o, color); // ATTENTION Input struct needs "screenPos"
			#endif // AURA
		}
//↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
//	END OF AURA 2 BLOCK																				↑↑
//↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

		uniform float _NormalScale;
		uniform sampler2D _ParticleNormalmap;
		uniform float4 _ParticleNormalmap_ST;
		uniform float3 _ColorPower;
		uniform float4 _Color;
		uniform sampler2D _ParticleTexture;
		uniform float4 _ParticleTexture_ST;
		uniform float _Metallic;
		uniform float _Smoothness;
		uniform float _AOPower;
		uniform float _Opacity;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_ParticleNormalmap = i.uv_texcoord * _ParticleNormalmap_ST.xy + _ParticleNormalmap_ST.zw;
			o.Normal = UnpackScaleNormal( tex2D( _ParticleNormalmap, uv_ParticleNormalmap ), _NormalScale );
			float2 uv_ParticleTexture = i.uv_texcoord * _ParticleTexture_ST.xy + _ParticleTexture_ST.zw;
			float4 tex2DNode34 = tex2D( _ParticleTexture, uv_ParticleTexture );
			o.Albedo = ( ( ( float4( _ColorPower , 0.0 ) * _Color ) * tex2DNode34 ) * i.vertexColor ).rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Smoothness;
			o.Occlusion = _AOPower;
			float clampResult40 = clamp( ( _Opacity * tex2DNode34.a ) , 0.0 , 1.0 );
			o.Alpha = ( i.vertexColor.a * clampResult40 );
		}

		ENDCG
	}
}