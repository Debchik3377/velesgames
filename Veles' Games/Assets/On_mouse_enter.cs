using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class On_mouse_enter : MonoBehaviour
{
    public GameObject panel;
    private void OnMouseEnter()
    {
        Debug.Log("enter");
        panel.SetActive(true);
    }
    private void OnMouseExit()
    {
        panel.SetActive(false);
    }
}
