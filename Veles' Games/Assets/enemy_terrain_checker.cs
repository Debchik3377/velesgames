using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy_terrain_checker : MonoBehaviour
{
    public List<TerrainLayer> grass;
    public Terrain terrain;
    public enemy_audio ea;
    private void Awake()
    {
        ea = GetComponent<enemy_audio>();
    }

    private float[] GetTextureMix(Vector3 playerpos)
    {
        Vector3 tpos = terrain.transform.position;
        TerrainData tdata = terrain.terrainData;

        int mx = (int)((playerpos.x - tpos.x) / tdata.size.x * tdata.alphamapHeight);
        int mz = (int)((playerpos.z - tpos.z) / tdata.size.z * tdata.alphamapWidth);

        float[,,] splatmap = tdata.GetAlphamaps(mx, mz, 1, 1);

        float[] mix = new float[splatmap.GetUpperBound(2) + 1];
        for (int i = 0; i < mix.Length; i++)
        {
            mix[i] = splatmap[0, 0, i];
        }
        return mix;
    }

    public string GetLayerName(Vector3 playerpos)
    {
        float[] mix = GetTextureMix(playerpos);
        float strongest = 0;
        int maxi = 0;
        for (int i = 0; i < mix.Length; i++)
        {
            if (mix[i] > strongest)
            {
                maxi = i;
                strongest = mix[i];
            }
        }
        return terrain.terrainData.terrainLayers[maxi].name;
    }

    public void ChangeFootsteps(Vector3 playerpos)
    {
        string layername = GetLayerName(playerpos);
        bool foundgrass = false;

        foreach (TerrainLayer ter in grass)
        {
            if (ter.name == layername)
            {
                ea.footsteps.setParameterByName("footsteps", 0);
                foundgrass = true; break;
            }

        }
        if (!foundgrass) ea.footsteps.setParameterByName("footsteps", 1);
    }
}
