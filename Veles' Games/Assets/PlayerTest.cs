using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTest : MonoBehaviour
{
    private CharacterController Controller;
    private Rigidbody rb;

    private float nextPosCurTime = 0f;
    public float nextPosTime = .15f;

    // Ahead movement
    [Range(0.1f, 5f)]
    private float HistoricalPositionDuration = 1f;
    [Range(0.001f, 1f)]
    private float HistoricalPositionInterval = 1f;

    private Queue<Vector3> HistoricalVelocities;
    private float LastPositionTime;
    private int MaxQueueSize;

    public Vector3 AverageVelocity
    {
        get
        {
            Vector3 average = Vector3.zero;
            foreach (Vector3 velocity in HistoricalVelocities)
            {
                average += velocity;
            }
            average.y = 0f;

            return average / HistoricalVelocities.Count;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Controller = GetComponent<CharacterController>();

        MaxQueueSize = Mathf.CeilToInt(1f / HistoricalPositionInterval * HistoricalPositionDuration);
        HistoricalVelocities = new Queue<Vector3>(MaxQueueSize);
    }

    // Update is called once per frame
    void Update()
    {
        if (LastPositionTime + HistoricalPositionInterval <= Time.time)
        {
            if (HistoricalVelocities.Count == MaxQueueSize)
            {
                HistoricalVelocities.Dequeue();
            }

            HistoricalVelocities.Enqueue(Controller.velocity);
            LastPositionTime = Time.time;
        }

        if (nextPosCurTime >= nextPosTime)
        {
            nextPosCurTime = 0f;
            float posX = Random.Range(-5, 5);
            float posz = Random.Range(-5, 5);
            Vector3 direction = new Vector3(posX, 0, posz);
           // Controller.Move(direction);
        }
        else
        {
            nextPosCurTime += Time.deltaTime;
        }
    }
}
