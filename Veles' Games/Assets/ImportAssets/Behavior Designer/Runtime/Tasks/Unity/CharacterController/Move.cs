using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

namespace BehaviorDesigner.Runtime.Tasks.Unity.UnityCharacterController
{
    [TaskCategory("Unity/CharacterController")]
    [TaskDescription("A more complex move function taking absolute movement deltas. Returns Success.")]
    public class Move : Action
    {
        [Tooltip("The GameObject that the task operates on. If null the task GameObject is used.")]
        public SharedGameObject targetGameObject;
        [Tooltip("The amount to move")]
        public SharedVector3 motion;
        public Animator animator;

        private NavMeshAgent agent;
        private GameObject prevGameObject;

        private Vector2 Velocity;
        private Vector2 SmoothDeltaPosition;

        public override void OnStart()
        {
            var currentGameObject = GetDefaultGameObject(targetGameObject.Value);
            animator = GetComponent<Animator>();
            animator.applyRootMotion = true;
            agent.updatePosition = false;
            agent.updateRotation = true;
            if (currentGameObject != prevGameObject) {
                agent = currentGameObject.GetComponent<NavMeshAgent>();
                prevGameObject = currentGameObject;
            }
        }

        

        public override TaskStatus OnUpdate()
        {
            if (agent == null) {
                Debug.LogWarning("CharacterController is null");
                return TaskStatus.Failure;
            }

            agent.Move(motion.Value);

            return TaskStatus.Success;
        }

        public override void OnReset()
        {
            targetGameObject = null;
            motion = Vector3.zero;
        }
    }
}