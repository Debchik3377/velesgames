using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class RootMotionBoss : MonoBehaviour
{
    public Camera Camera;
    public LayerMask LayerMask;
    public NavMeshAgent agent;
    public Animator animator;
    public Vector2 velocity;
    private Vector2 SmoothDeltaPosition;

    private int animIDMove;
    private int animIDVelocity;

    // Start is called before the first frame update
    void Start()
    {
        animIDMove = Animator.StringToHash("Move");
        animIDVelocity = Animator.StringToHash("Velocity");

        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        animator.applyRootMotion = false;
        agent.updatePosition = false;
        agent.updateRotation = true;
    }

    private void OnAnimatorMove()
    {
        Vector3 rootPosition = animator.rootPosition;
        rootPosition.y = agent.nextPosition.y;  
        transform.position = rootPosition;
        agent.nextPosition = rootPosition; 
    }

    private void SyncronizeAnimatorAndAgent()
    {
        Vector3 worldDeltaPosition = agent.nextPosition - transform.position;
        worldDeltaPosition.y = 0;

        float dx = Vector3.Dot(transform.right, worldDeltaPosition);
        float dz = Vector3.Dot(transform.forward, worldDeltaPosition);

        Vector2 deltaPosition = new Vector2(dx, dz);

        float smooth = Mathf.Min(1, Time.deltaTime / 0.1f);
        Debug.Log(smooth);
        SmoothDeltaPosition = Vector2.Lerp(SmoothDeltaPosition, deltaPosition, smooth);
        Debug.Log(SmoothDeltaPosition);


        velocity = SmoothDeltaPosition / Time.deltaTime;
        if (agent.remainingDistance <= agent.stoppingDistance)
        {
            velocity = Vector2.Lerp(
                Vector2.zero,
                velocity, 
                agent.remainingDistance / agent.stoppingDistance
                );
        }

        bool shouldMove = velocity.magnitude > 0.5f
            && agent.remainingDistance > agent.stoppingDistance;

        animator.SetBool(animIDMove, shouldMove);
        animator.SetFloat(animIDVelocity, velocity.magnitude);

        float deltaMagnitude = worldDeltaPosition.magnitude;
        if (deltaMagnitude > agent.radius / 2f)
        {
            transform.position = Vector3.Lerp(
                animator.rootPosition, 
                agent.nextPosition, 
                smooth
                );
        }

        
    }

    private void Test()
    {
        animator.SetBool(animIDMove, agent.velocity.magnitude > 0.5f);
        animator.SetFloat(animIDVelocity, agent.velocity.magnitude);
    }

    private void HandleInput()
    {
        if (Application.isFocused && Mouse.current.leftButton.wasPressedThisFrame)
        {
            Ray ray = Camera.ScreenPointToRay(Mouse.current.position.ReadValue());

            if (Physics.Raycast(ray, out RaycastHit hit, float.MaxValue, LayerMask))
            {
                Debug.Log(hit.point);
                agent.SetDestination(hit.point);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        HandleInput();
        SyncronizeAnimatorAndAgent();
    }
}
