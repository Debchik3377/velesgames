using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using StarterAssets;

public class Npc_trade_item : MonoBehaviour
{
    [Header("Item")]
    public int price;
    public int count;
    public int index;
    public ItemBase trade_item;
    public GameObject icon;

    [Header ("Other")]
    public TMP_Text item_name;
    public TMP_Text countText;
    public TMP_Text priceText;

    UI_lean_tween statsManager;
    InventoryManager inventory;

    public NPS_Input_controller npc_input;
    Npc_activate npc;
    public PlayerStats playerStats;

    // Start is called before the first frame update
    void Awake()
    {
        inventory = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();
        statsManager = GameObject.Find("UIStatsManager").GetComponent<UI_lean_tween>();
        npc_input = ThirdPersonController.instance.gameObject.GetComponent<NPS_Input_controller>();
    }

    public void Init(ItemBase item, int pr, int c, int i)
    {
        npc = npc_input.current_nps.GetComponent<Npc_activate>();
        price = pr;
        trade_item = item;
        count = c;
        index = i;
        countText.text = count.ToString();
        priceText.text = price.ToString();
        icon.GetComponent<Image>().sprite = trade_item.image;
        item_name.text = trade_item.name;
    }

    public void Trade()
    {
        if (playerStats.hearts_count >= price)
        {
            statsManager.Add_hearts(-price);
            inventory.AddItem(trade_item);
            count -= 1;
            npc.counts[index] -= 1;
            countText.text = count.ToString();
            if (count == 0) Destroy(gameObject);
        }
    }
}
