using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class NPS_Input_controller : MonoBehaviour
{
    public GameObject choice_panel_dialog;
    public GameObject choice_panel_deal;
    public GameObject dialog_panel;
    public GameObject activate;
    public GameObject deal_panel;
    public GameObject deal_back;
    public TextMeshProUGUI dialog_text;
    public GameObject trade_item;

    public float word_speed = 0.05f;

    public string[] dialog_list;
    public ItemBase[] trade_items;
    public int[] prices;
    public int[] counts;
    int dialog_index = 0;

    Animator nps_animator;
    public GameObject current_nps;

    public GameObject fpcamera;
    public GameObject nps_look_at = null;
    public GameObject active_slots;
    public GameObject gameplay_UI;

    bool typing = false;
    public bool in_range = false;

    private int _in_choice;
    private int _nps_action;
    private int _deal;
    private int _dialog;

    PlayerInput playerInput;

    private StarterAssetsInputs _input;

    void Start()
    {
        playerInput = GetComponent<PlayerInput>();
        _input = GetComponent<StarterAssetsInputs>();
    }

    public void ZeroDeals()
    {
        trade_items = null;
        prices = null;
        counts = null;
    }

    public void FillDeal()
    {
        for (int i = 0; i < deal_back.transform.childCount; i++) Destroy(deal_back.transform.GetChild(i).gameObject);
        for (int i = 0; i < trade_items.Length; i++)
        {
            if (counts[i] > 0)
            {
                GameObject trade = Instantiate(trade_item, deal_back.transform);
                Npc_trade_item _Trade_Item = trade.GetComponent<Npc_trade_item>();
                _Trade_Item.Init(trade_items[i], prices[i], counts[i], i);
            }
        }
    }

    private void OnNpsActivate()
    {
        if (in_range)
        {
            nps_animator = current_nps.GetComponent<Animator>();

            nps_animator.SetBool(_in_choice, true);
            activate.SetActive(false);

            if (dialog_list.Length != 0) choice_panel_dialog.SetActive(true);
            if (trade_items.Length != 0) choice_panel_deal.SetActive(true);

            active_slots.SetActive(false);
            gameplay_UI.SetActive(false);
            fpcamera.SetActive(true);
            GetComponent<Animator>().SetBool(_nps_action, true);

            fpcamera.GetComponent<CinemachineVirtualCamera>().LookAt = nps_look_at.transform;

            transform.LookAt(current_nps.transform.position);

            current_nps.transform.LookAt(transform.position);

            Zero_text();

            playerInput.SwitchCurrentActionMap("nps");
        }

    }

    private void OnDeal()
    {
        if (trade_items.Length != 0)
        {
            nps_animator.SetBool(_in_choice, false);
            nps_animator.SetBool(_deal, true);
            choice_panel_dialog.SetActive(false);
            choice_panel_deal.SetActive(false);
            deal_panel.SetActive(true);
            FillDeal();
            _input.SetCursorState(false);
            playerInput.SwitchCurrentActionMap("nps_deal");
        }
    }

    private void OnDialog()
    {
        if (dialog_list.Length != 0)
        {
            nps_animator.SetBool(_in_choice, false);
            nps_animator.SetBool(_dialog, true);
            choice_panel_dialog.SetActive(false);
            choice_panel_deal.SetActive(false);
            dialog_panel.SetActive(true);
            Next_line();
            playerInput.SwitchCurrentActionMap("nps_dialog");
        }
    }

    private void OnExit()
    {
        nps_animator.SetBool(_in_choice, false);
        activate.SetActive(true);
        choice_panel_dialog.SetActive(false);
        choice_panel_deal.SetActive(false);
        GetComponent<Animator>().SetBool(_nps_action, false);
        fpcamera.SetActive(false);
        active_slots.SetActive(true);
        gameplay_UI.SetActive(true);
        playerInput.SwitchCurrentActionMap("Player");
    }

    private void OnNextLine()
    {
        if (!typing) Next_line();
        else
        {
            StopAllCoroutines();
            dialog_text.text = dialog_list[dialog_index - 1];
            typing = false;
        }
    }

    private void OnDialogExit()
    {
        dialog_index = 0;
        _input.SetCursorState(true);
        nps_animator.SetBool(_in_choice, true);
        nps_animator.SetBool(_deal, false);
        nps_animator.SetBool(_dialog, false);
        deal_panel.SetActive(false);
        dialog_panel.SetActive(false);
        if (dialog_list.Length != 0) choice_panel_dialog.SetActive(true);
        if (trade_items.Length != 0) choice_panel_deal.SetActive(true);
        playerInput.SwitchCurrentActionMap("nps");
    }

    public void Next_line()
    {
        if (dialog_index < dialog_list.Length)
        {
            dialog_text.text = "";
            StartCoroutine(Typing());
            dialog_index++;
        }
        else
        {
            Zero_text();
        }
    }

    IEnumerator Typing()
    {
        typing = true;
        foreach (char letter in dialog_list[dialog_index].ToCharArray())
        {
            dialog_text.text += letter;
            yield return new WaitForSeconds(word_speed);
        }
        typing = false;
    }

    public void Zero_text()
    {
        dialog_text.text = "";
        dialog_index = 0;
        dialog_panel.SetActive(false);
    }

    private void AssignAnimatorIDs()
    {
        _in_choice = Animator.StringToHash("_in_choice");
        _nps_action = Animator.StringToHash("nps_action");
        _deal = Animator.StringToHash("deal");
        _dialog = Animator.StringToHash("dialog");
    }
}
