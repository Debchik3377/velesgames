using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Npc_activate : MonoBehaviour
{
    Animator animator;
    public GameObject nps_look_at;
    GameObject player;
    NPS_Input_controller nps_input;

    public string[] dialog_list;
    public ItemBase[] trade_items;
    public int[] prices;
    public int[] counts;

    private int _in_choice;
    private int _deal;
    private int _dialog;
    void Start()
    {
        AssignAnimatorIDs();
        animator = GetComponent<Animator>();
        player = GameObject.Find("Player");
        nps_input = ThirdPersonController.instance.gameObject.GetComponent<NPS_Input_controller>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            nps_input.trade_items = trade_items;
            nps_input.prices = prices;
            nps_input.counts = counts;
            nps_input.current_nps = gameObject;
            nps_input.nps_look_at = nps_look_at;
            nps_input.in_range = true;
            nps_input.activate.SetActive(true);
            nps_input.dialog_list = dialog_list;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            nps_input.ZeroDeals();
            nps_input.Zero_text();
            animator.SetBool(_deal, false);
            animator.SetBool(_dialog, false);
            animator.SetBool(_in_choice, false);
            nps_input.in_range = false;
            nps_input.activate.SetActive(false);
            nps_input.trade_items = null;
            nps_input.prices = null;
            nps_input.counts = null;
        }
    }

    private void AssignAnimatorIDs()
    {
        _in_choice = Animator.StringToHash("in_choice");
        _deal = Animator.StringToHash("deal");
        _dialog = Animator.StringToHash("dialog");
    }
}
