using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour
{
    //private WeaponType weaponType;
    private bool canDealDamage;
    List<GameObject> hasDealtDamage;
    PlayerStats playerStats;

    [SerializeField] public float weaponDamage;
    [SerializeField] string target;
    [SerializeField] LayerMask attacking_layer;
    [SerializeField] GameObject bloodBurst;


    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(gameObject);

        canDealDamage = false;
        hasDealtDamage = new List<GameObject>();
        if (target == "Enemy")
        {
            playerStats = ThirdPersonController.instance.playerStats;
            bloodBurst =ThirdPersonController.instance.bloodBurst;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (canDealDamage)
        {
            //Debug.Log("IN");
            if (target == "Enemy" && other.transform.TryGetComponent<Enemy_stats>(out Enemy_stats enemy) && !hasDealtDamage.Contains(other.gameObject))
            {
                //Debug.Log("Damage From Player");
                
                enemy.TakeDamage(weaponDamage * playerStats.damageMultiplier);
                hasDealtDamage.Add(other.gameObject);
            }

            if (target == "Enemy" && other.transform.root.TryGetComponent<BossMono>(out BossMono boss) && !hasDealtDamage.Contains(other.gameObject))
            {
                if (boss.dead)
                {
                    boss.death.Invoke(); //������������ ��������
                    return;
                }

                Debug.Log("Hit boss");
                boss.GetDamage(weaponDamage);
                hasDealtDamage.Add(other.gameObject);
            }

            if (target == "Player" && other.transform.TryGetComponent<PlayerStatistics>(out PlayerStatistics player) && !hasDealtDamage.Contains(other.gameObject))
            {
                Debug.Log("Damage From Boss  -  " + gameObject);
                player.TakeDamage(weaponDamage);
                hasDealtDamage.Add(other.gameObject);
            }

        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (canDealDamage)
        {
            //Debug.Log(other.transform.root.GetChild(0));
            if (target == "Enemy" && other.transform.TryGetComponent<Enemy_stats>(out Enemy_stats enemy) && !hasDealtDamage.Contains(other.gameObject))
            {
                //Debug.Log("Damage From Player");

                enemy.TakeDamage(weaponDamage * playerStats.damageMultiplier);
                hasDealtDamage.Add(other.gameObject);
            }

            if (target == "Enemy" && other.transform.root.TryGetComponent<BossMono>(out BossMono boss) && !hasDealtDamage.Contains(other.gameObject))
            {
                Debug.Log(boss);
                Debug.Log("Hit boss");
                boss.GetDamage(weaponDamage);
                hasDealtDamage.Add(other.gameObject);
            }

            if (target == "Player" && other.transform.TryGetComponent<PlayerStatistics>(out PlayerStatistics player) && !hasDealtDamage.Contains(other.gameObject))
            {
                Debug.Log("Damage From Boss  -  " + gameObject);
                player.TakeDamage(weaponDamage);
                hasDealtDamage.Add(other.gameObject);
            }

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.tag.Contains("Enemy"))
        {
            GameObject blood = Instantiate(bloodBurst, collision.contacts[0].otherCollider.transform);
            StartCoroutine(DestroyBlood(blood));
        }
    }

    IEnumerator DestroyBlood(GameObject toDestroy)
    {
        yield return new WaitForSeconds(3);
        Destroy(toDestroy);
    }

    public void StartDealDamage()
    {
        canDealDamage = true;
        hasDealtDamage.Clear();
    }

    public void EndDealDamage()
    {
        canDealDamage = false;
    }
}