using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

public class Roll : StateMachineBehaviour
{
    [SerializeField] private ThirdPersonController playerController;
    [SerializeField] private StarterAssetsInputs _inputs;

    [SerializeField] private float recoverStaminaValue;
    [SerializeField] private float recoverStaminaMultiplier;

    private void Awake()
    {
        GameObject player = GameObject.Find("Player");
        _inputs = player.GetComponent<StarterAssetsInputs>();
        playerController = player.GetComponent<ThirdPersonController>();
    }
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        playerController.recoverStaminaValue = recoverStaminaValue;
        playerController.recoverStaminaMultiplier = recoverStaminaMultiplier;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
        //playerController.MoveVertically();
        //playerController.ApplyRootMotion();
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        playerController.UnCallRoll();
    }

    override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.transform.position = animator.rootPosition;
        ThirdPersonController.instance._controller.transform.position = animator.transform.position;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
