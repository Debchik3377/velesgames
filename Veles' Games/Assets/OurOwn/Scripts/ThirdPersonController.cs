﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting.Antlr3.Runtime.Misc;
using UnityEditor;
using UnityEditor.Rendering;
using UnityEngine;
using FMOD.Studio;
#if ENABLE_INPUT_SYSTEM
using UnityEngine.InputSystem;
using UnityEngine.Windows;
#endif

/* Note: animations are called via the controller for both the character and capsule using animator null checks
 */

namespace StarterAssets
{
    [RequireComponent(typeof(CharacterController))]
#if ENABLE_INPUT_SYSTEM
    [RequireComponent(typeof(PlayerInput))]
#endif
    public class ThirdPersonController : MonoBehaviour
    {
        [Header("Player")]
        [Tooltip("Move speed of the character in m/s")]
        public float MoveSpeed = 2.0f;

        [Tooltip("Sprint speed of the character in m/s")]
        public float SprintSpeed = 5.335f;

        [Tooltip("How fast the character turns to face movement direction")]
        [Range(0.0f, 0.3f)]
        public float RotationSmoothTime = 0.12f;

        [Tooltip("Acceleration and deceleration")]
        public float SpeedChangeRate = 10.0f;

        public AudioClip LandingAudioClip;
        public AudioClip[] FootstepAudioClips;
        [Range(0, 1)] public float FootstepAudioVolume = 0.5f;

        [Space(10)]
        [Tooltip("The height the player can jump")]
        public float JumpHeight = 1.2f;

        [Tooltip("The character uses its own gravity value. The engine default is -9.81f")]
        public float Gravity = -15.0f;

        [Space(10)]
        [Tooltip("Time required to pass before being able to jump again. Set to 0f to instantly jump again")]
        public float JumpTimeout = 0.50f;

        [Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
        public float FallTimeout = 0.15f;

        [Header("Player Grounded")]
        [Tooltip("If the character is grounded or not. Not part of the CharacterController built in grounded check")]
        public bool Grounded = true;

        [Tooltip("Useful for rough ground")]
        public float GroundedOffset = -0.14f;

        [Tooltip("The radius of the grounded check. Should match the radius of the CharacterController")]
        public float GroundedRadius = 0.28f;

        [Tooltip("What layers the character uses as ground")]
        public LayerMask GroundLayers;

        [Header("Cinemachine")]
        [Tooltip("The follow target set in the Cinemachine Virtual Camera that the camera will follow")]
        public GameObject CinemachineCameraTarget;

        [Tooltip("How far in degrees can you move the camera up")]
        public float TopClamp = 70.0f;

        [Tooltip("How far in degrees can you move the camera down")]
        public float BottomClamp = -30.0f;

        [Tooltip("Additional degress to override the camera. Useful for fine tuning camera position when locked")]
        public float CameraAngleOverride = 0.0f;

        [Tooltip("For locking the camera position on all axis")]
        public bool LockCameraPosition = false;

        [Header("UI")]
        public UI_lean_tween ui_stats;
        // cinemachine
        private float _cinemachineTargetYaw;
        private float _cinemachineTargetPitch;
        public GameObject bloodBurst;

        // player
        public float _speed;
        private float _animationBlend;
        private float _targetRotation = 0.0f;
        private float _rotationVelocity;
        private float _verticalVelocity;
        private float _terminalVelocity = 53.0f;

        // timeout deltatime
        private float _jumpTimeoutDelta;
        private float _fallTimeoutDelta;

        // animation IDs
        private int _animIDSpeed;
        private int _animIDGrounded;
        private int _animIDJump;
        private int _animIDFreeFall;
        private int _animIDMotionSpeed;
        private int _animIDRoll;
        private int _animIDHit;
        private int _animIDEquipOH;
        private int _animIDEquipTH;
        private int _animIDUnEquipOH;
        private int _animIDUnEquipTH;
        private int _animIDUnBusyOH;
        private int _animIDUnBusyTH;
        private int _animIDHitCount;
        private int _animIDChangeWeapon;
        private int _animIDMoving;

#if ENABLE_INPUT_SYSTEM
        private PlayerInput _playerInput;
        private PlayerBehaviour _behaviour;
#endif
        public Animator _animator;
        public CharacterController _controller;
        private StarterAssetsInputs _input;
        private GameObject _mainCamera;
        public InventoryManager inventoryManager;

        public float standHeight = 1.8f;
        public float fallHeight = 1.4f;
        public float rollHeight = 1.0f;

        private const float _threshold = 0.01f;

        private bool _hasAnimator;

        [Header("For stamina system")]
        [SerializeField] public float recoverStaminaValue;
        [SerializeField] public float recoverStaminaMultiplier;
        [Range(0f, 2f)]
        [SerializeField] private float runningSpendMultiplier;
        public float delta;


        // For Boss
        private float nextPosCurTime = 0f;
        public float nextPosTime = .15f;

        // Ahead movement
        [Range(0.1f, 5f)]
        [SerializeField]
        private float HistoricalPositionDuration = 1f;
        [Range(0.001f, 1f)]
        [SerializeField]
        private float HistoricalPositionInterval = 1f;

        public Queue<Vector3> HistoricalVelocities;
        private float LastPositionTime;
        public int MaxQueueSize;
        public bool inAction;

        public static ThirdPersonController instance;

        public Vector3 AverageVelocity
        {
            get
            {
                Vector3 average = Vector3.zero;
                foreach (Vector3 velocity in HistoricalVelocities)
                {
                    average += velocity;
                }
                average.y = 0f;

                return average / HistoricalVelocities.Count;
            }
        }

        private bool IsCurrentDeviceMouse
        {
            get
            {
#if ENABLE_INPUT_SYSTEM
                return _playerInput.currentControlScheme == "KeyboardMouse";
#else
				return false;
#endif
            }
        }

        public float recoverStaminaRate = .5f;
        public float lastRecover = 0.0f;

        public void RecoverStamina(float baseValue = 1, float multiplier = 1)
        {
            if (playerStats.stamina < 100f && Time.time - lastRecover > recoverStaminaRate)
            {
                //if (baseValue == 0) baseValue = playerStats.staminaRecover;

                float target_stamina = playerStats.stamina + baseValue * multiplier;
                target_stamina = Mathf.Clamp(target_stamina, 0, 100);
               

                float st = Mathf.SmoothDamp(playerStats.stamina, target_stamina, ref baseValue, Time.deltaTime);
                ui_stats.Set_st(st);
                playerStats.stamina = st;

                lastRecover = Time.time;
            }
        }

        private void Update()
        {
            if (dead) return;

            delta += Time.deltaTime;
            GroundedCheck();
            JumpAndGravity();
            ResetHits();
            VelocityQueue();

            RecoverStamina(recoverStaminaValue, recoverStaminaMultiplier);

        }


        private int framesToReload = 0;
        private void LateUpdate()
        {
            if (!_inventory)
                CameraRotation();

            framesToReload ++;
            if (framesToReload > 2f)
            {
                _input.pickUp = false;
                framesToReload = 0;
            }
        }

        private void Awake()
        {
            if (instance == null)
                instance = this;

            // get a reference to our main camera
            if (_mainCamera == null)
            {
                _mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
            }
            equipping = false;
            busyOH = false;
            busyTH = false;
        }

        private void Start()
        {
            playerStats.stamina = 100f;
            Audiomanager.instance.breath.start();
            _cinemachineTargetYaw = CinemachineCameraTarget.transform.rotation.eulerAngles.y;
            //_behaviour = GetComponent<PlayerBehaviour>();
            _hasAnimator = TryGetComponent(out _animator);
            _controller = GetComponent<CharacterController>();
            _input = GetComponent<StarterAssetsInputs>();
#if ENABLE_INPUT_SYSTEM
            _playerInput = GetComponent<PlayerInput>();
            inventoryManager = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();
            equipmentSystem = GetComponent<EquipmentSystem>();
#else
			//Debug.LogError( "Starter Assets package is missing dependencies. Please use Tools/Starter Assets/Reinstall Dependencies to fix it");
#endif
            MaxQueueSize = Mathf.CeilToInt(1f / HistoricalPositionInterval * HistoricalPositionDuration);
            HistoricalVelocities = new Queue<Vector3>(MaxQueueSize);
            AssignAnimationIDs();

            // reset our timeouts on start
            _jumpTimeoutDelta = JumpTimeout;
            _fallTimeoutDelta = FallTimeout;
        }

        public int _animIDPotion;
        private void AssignAnimationIDs()
        {
            _animIDSpeed = Animator.StringToHash("Speed");
            _animIDGrounded = Animator.StringToHash("Grounded");
            _animIDJump = Animator.StringToHash("Jump");
            _animIDFreeFall = Animator.StringToHash("FreeFall");
            _animIDMotionSpeed = Animator.StringToHash("MotionSpeed");
            _animIDRoll = Animator.StringToHash("Roll");
            _animIDHit = Animator.StringToHash("Hit");
            _animIDEquipOH = Animator.StringToHash("EquipOH");
            _animIDEquipTH = Animator.StringToHash("EquipTH");
            _animIDUnEquipOH = Animator.StringToHash("UnequipOH");
            _animIDUnEquipTH = Animator.StringToHash("UnequipTH");
            _animIDUnBusyOH = Animator.StringToHash("BusyOH");
            _animIDUnBusyTH = Animator.StringToHash("BusyTH");
            _animIDHitCount = Animator.StringToHash("HitCount");
            _animIDChangeWeapon = Animator.StringToHash("ChangeWeapon");
            _animIDMoving = Animator.StringToHash("isMoving");
            _animIDPotion = Animator.StringToHash("Potion");
        }

        private void VelocityQueue()
        {
            if (LastPositionTime + HistoricalPositionInterval <= Time.time)
            {
                Debug.Log("Written");
                if (HistoricalVelocities.Count == MaxQueueSize)
                {
                    HistoricalVelocities.Dequeue();
                }

                HistoricalVelocities.Enqueue(_controller.velocity);
                LastPositionTime = Time.time;
            }

            if (nextPosCurTime >= nextPosTime)
            {
                nextPosCurTime = 0f;
                float posX = Random.Range(-5, 5);
                float posz = Random.Range(-5, 5);
                Vector3 direction = new Vector3(posX, 0, posz);
                // Controller.Move(direction);
            }
            else
            {
                nextPosCurTime += Time.deltaTime;
            }
        }

        

        public void CameraRotation()
        {
            //Debug.Log("Rotating");
            // if there is an input and camera position is not fixed
            if (_input.look.sqrMagnitude >= _threshold && !LockCameraPosition)
            {
                //Don't multiply mouse input by Time.deltaTime;
                float deltaTimeMultiplier = IsCurrentDeviceMouse ? 1.0f : Time.deltaTime;

                _cinemachineTargetYaw += _input.look.x * deltaTimeMultiplier;
                _cinemachineTargetPitch += _input.look.y * deltaTimeMultiplier;
            }

            // clamp our rotations so our values are limited 360 degrees
            _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
            _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

            // Cinemachine will follow this target
            CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch + CameraAngleOverride,
                _cinemachineTargetYaw, 0.0f);
        }

        [Header("Buffs")]
        public bool Untiring; // stamina doesn't spend
        [Range(1, 100)]
        public float heartMultiplier;
        public bool Berserk;
        public PotionEffect currentPotion;
        public InventoryItem BuffItem;
        public InventorySlot BuffSlot;

        public bool BuffIsNotEmpty()
        {
            return BuffItem != null;
        }

        public void PotionEffect()
        {
            currentPotion.TakeBuff();
        }

        public void BuffPlayer()
        {
            if (_animator.GetCurrentAnimatorStateInfo(2).IsTag("Buff"))
                return;
            
            if (BuffIsNotEmpty())
            {
                Debug.Log(currentPotion);
                equipmentSystem.PutBuffInHand();
                currentPotion = equipmentSystem.buffInHands.GetComponent<PotionEffect>();
                currentPotion.StartTakinngBuff();
                BuffItem.count--;
                BuffItem.RefreshCount();
                if (BuffItem.count <= 0)
                {
                    Debug.Log(BuffItem);
                    Destroy(inventoryManager.buffSlot.transform.GetChild(0).gameObject);
                    BuffItem = null;
                }
            }
            
        }

        public void StaminaUnStopable(float time)
        {
            StopCoroutine("StaminaContinue");
            StartCoroutine(StaminaContinue(time));
            Untiring = true;
        }

        public IEnumerator StaminaContinue(float time)
        {
            Debug.Log("Started");
            yield return new WaitForSeconds(time);
            Debug.Log("StoppedStamina");
            Untiring = false;
        }

        public void HeartMultiplier(float hearts)
        {
            StopCoroutine("ReturnHearts");
            StartCoroutine(ReturnHearts(hearts));
            heartMultiplier = hearts;
        }

        private IEnumerator ReturnHearts(float hearts)
        {
            yield return new WaitForSeconds(hearts);
            heartMultiplier = 1;
        }

        public void BecomeBerserk(float mult, float time)
        {
            StopCoroutine("UncallBerserk");
            StartCoroutine(UncallBerserk(time));
            playerStats.damageMultiplier = mult;
        }

        private IEnumerator UncallBerserk(float time)
        {
            yield return new WaitForSeconds(time);
            playerStats.damageMultiplier = 1;
        }

        public bool canRotate = true;

        public void Move()
        {
            if (dead) return;
            // set target speed based on move speed, sprint speed and if sprint is pressed

            float targetSpeed = MoveSpeed;
            if (_input.sprint && _speed > 0.75f * MoveSpeed && playerStats.stamina - runningSpendMultiplier * recoverStaminaValue > 0)
            {
                targetSpeed = SprintSpeed;
                if (!Untiring)
                    playerStats.SpendStamina(runningSpendMultiplier * recoverStaminaValue);
            }

            if (_inventory)
            {
                _speed = 0f;
                _animator.SetFloat(_animIDSpeed, _speed);
                _animator.SetBool(_animIDMoving, false);
                targetSpeed = 0;
            }

            // a simplistic acceleration and deceleration designed to be easy to remove, replace, or iterate upon

            // note: Vector2's == operator uses approximation so is not floating point error prone, and is cheaper than magnitude
            // if there is no input, set the target speed to 0
            if (_input.move == Vector2.zero) targetSpeed = 0.0f;

            // a reference to the players current horizontal velocity
            float currentHorizontalSpeed = new Vector3(_controller.velocity.x, 0.0f, _controller.velocity.z).magnitude;

            float speedOffset = 0.1f;
            float inputMagnitude = _input.analogMovement ? _input.move.magnitude : 1f;


            if (inAction)
                targetSpeed = 0;
            // accelerate or decelerate to target speed
            if (currentHorizontalSpeed < targetSpeed - speedOffset ||
                currentHorizontalSpeed > targetSpeed + speedOffset)
            {
                // creates curved result rather than a linear one giving a more organic speed change
                // note T in Lerp is clamped, so we don't need to clamp our speed
                
                _speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed * inputMagnitude,
                    Time.deltaTime * SpeedChangeRate);

                // round speed to 3 decimal places
                _speed = Mathf.Round(_speed * 1000f) / 1000f;
            }
            else
            {
                _speed = targetSpeed;
            }

            if (_speed >= SprintSpeed) Audiomanager.instance.breath.setParameterByName("breath", 1);
            else Audiomanager.instance.breath.setParameterByName("breath", 0);

            if (_speed == 0f)
            {
                _animator.SetBool(_animIDMoving, false);
            }
            else
            {
                _animator.SetBool(_animIDMoving, true);
            }

            _animationBlend = Mathf.Lerp(_animationBlend, targetSpeed, Time.deltaTime * SpeedChangeRate);
            if (_animationBlend < 0.01f) _animationBlend = 0f;

            // normalise input direction
            Vector3 inputDirection = new Vector3(_input.move.x, 0.0f, _input.move.y).normalized;

            // note: Vector2's != operator uses approximation so is not floating point error prone, and is cheaper than magnitude
            // if there is a move input rotate player when the player is moving
            if (_input.move != Vector2.zero && !inAction && !_inventory && canRotate)
            {
                

                _targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg +
                                  _mainCamera.transform.eulerAngles.y;
                float rotation = Mathf.SmoothDampAngle(_animator.gameObject.transform.eulerAngles.y, _targetRotation, ref _rotationVelocity,
                    RotationSmoothTime);

                // rotate to face input direction relative to camera position
                _animator.gameObject.transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);
            }

           

            Vector3 targetDirection = Quaternion.Euler(0.0f, _targetRotation, 0.0f) * Vector3.forward;
            // move the player
            _controller.Move(targetDirection.normalized * (_speed * Time.deltaTime) +
                             new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);

            if (_speed >= SprintSpeed) Audiomanager.instance.breath.setParameterByName("breath", 1);
            else Audiomanager.instance.breath.setParameterByName("breath", 0);
            // update animator if using character
            if (_hasAnimator)
            {
                _animator.SetFloat(_animIDSpeed, _animationBlend);
                _animator.SetFloat(_animIDMotionSpeed, inputMagnitude);
            }
        }


        public void MoveVertically()
        {
            //Debug.Log(_verticalVelocity);
            _controller.Move(new Vector3(0, _verticalVelocity, 0) * Time.deltaTime);
        }

        public void ResetValues()
        {
            _animator.SetBool(_animIDRoll, false);

        }

        // Перекаты - Следующие три функции
        [SerializeField] private Transform rollCheck;

        public bool canRoll;
        public void CallRoll()
        {
            //RaycastHit hit;
            canRoll = true;//!Physics.BoxCast(transform.position + new Vector3(0, 1f, 1f), new Vector3(3f, 1f, 3f), transform.forward, Quaternion.identity, 1f);
            if (canRoll)
            {
                _input.roll = false;
                _animator.applyRootMotion = true;
                _animator.SetBool(_animIDRoll, true);
            }
            else
            {
                _input.roll = false;
            }
            
            //_controller.height = rollHeight;
        }

        public void UnCallRoll()
        {
            _input.roll = false;
            _animator.applyRootMotion = false;
            _animator.SetBool(_animIDRoll, false);
            //_controller.height = standHeight;
        }

        public void ApplyRootMotion()
        {
            Vector3 deltaPosition = _animator.deltaPosition;
            deltaPosition.y = 0.0f;
            _controller.Move(deltaPosition);
        }

        // Конец перекатов

        // Экипировка
        public bool equipping;
        public bool busyOH;
        public bool busyTH;

        public bool _inventory = false;
        [SerializeField] private GameObject inventoryPlane;
        [SerializeField] public EquipmentSystem equipmentSystem;
        [SerializeField] public ItemBase currentWeapon;

        private float curVel;
        public void OpenCloseInventory()
        {
            _speed = 0f;
            _animator.SetFloat(_animIDSpeed, 0f);
            _animator.SetBool(_animIDMoving, false);
            _inventory = !_inventory;
            inventoryPlane.SetActive(!inventoryPlane.activeInHierarchy);
            _input.SetCursorState(!_inventory);
        }

        public void PutOutInItem()
        {
            if (inventoryManager.weaponSlot.transform.childCount != 0)
            {
                currentWeapon = inventoryManager.weaponSlot.transform.GetChild(0).GetComponent<InventoryItem>().item;
               if (currentWeapon.weaponType == WeaponType.OneHand)
               {
                    if (busyOH)
                    {
                        _animator.SetTrigger(_animIDUnEquipOH);
                    }
                    else
                    {
                        Debug.Log(currentWeapon);
                        _animator.SetTrigger(_animIDEquipOH);
                    }
                    busyOH = !busyOH;
                    _animator.SetBool(_animIDUnBusyOH, busyOH);
               }
               else if (currentWeapon.weaponType == WeaponType.TwoHand)
                {
                    if (busyTH)
                    {
                        _animator.SetTrigger(_animIDUnEquipTH);
                    }
                    else
                    {
                        _animator.SetTrigger(_animIDEquipTH);
                    }
                    busyTH = !busyTH;
                    _animator.SetBool(_animIDUnBusyTH, busyTH);
                }

                
            }

        }

        public void SetBusyOH(bool value)
        {
            busyOH = value;
            _animator.SetBool(_animIDUnBusyOH, value);
        }

        public void SetBusyTH(bool value)
        {
            busyTH = value;
            _animator.SetBool(_animIDUnBusyTH, value);
        }

        public void PlayChanging()
        {
            _animator.SetTrigger(_animIDChangeWeapon);
        }

        public GameObject newWeaponItem = null;
        public void SetNewWeaponAnim()
        {
            currentWeapon = inventoryManager.weaponSlot.transform.GetChild(0).GetComponent<InventoryItem>().item;
            equipmentSystem.currentWeaponInHand = Instantiate(currentWeapon.Model, equipmentSystem.weaponHolder.transform);
        }

        public void ReleaseWeapon()
        {
            Destroy(equipmentSystem.currentWeaponInHand);
            equipmentSystem.currentWeaponInHand = null;
        }

        // Экипировка - конец

        // Информация об игроке 

        public PlayerStats playerStats;
        public bool dead = false;

        public void SetCheckpoint(Vector3 newCheckpointPosition)
        {
            playerStats.position = newCheckpointPosition;
        }

        public void ResetPosition()
        {
            Debug.Log("Resetting position");
            ui_stats.Set_hp(playerStats.max_health);
            ui_stats.Set_st(playerStats.max_stamina);
            _controller.transform.position = playerStats.position;
        }

        // Информация - конец


        // Удары
        [SerializeField] private string[] startAttacksOH;
        [SerializeField] private string[] middleAttacksOH;
        [SerializeField] private string[] finisherAttacksOH;
        [SerializeField] private string[] startAttacksTH;
        [SerializeField] private string[] middleAttacksTH;
        [SerializeField] private string[] finisherAttacksTH;
        [SerializeField] private float nextHitTime = 0f;
        [SerializeField] public int hitCount = 0;
        public bool attacking;
        public bool canAttack;

        private void ResetHits()
        {
            nextHitTime -= Time.deltaTime;
            if (hitCount > 0 && nextHitTime < 0)
            {
                Debug.Log("Nulled hits");
                //_animator.SetBool(_animIDHit, false);
                hitCount = 0;
                _animator.SetInteger(_animIDHitCount, hitCount);
            }
        }

        public void EndCombo()
        {
            hitCount = 0;
        }

        public void ContinueCombo()
        {
            canAttack = true;
            hitCount += 1;
            Debug.Log($"Hit count is {hitCount}");
        }

        public void StartHit()
        {
            int ind = 0;
            if (busyOH)
            {
                if (hitCount == 0 && canAttack)
                {
                    ind = Random.Range(0, startAttacksOH.Length);
                    _animator.Play(startAttacksOH[ind]);
                    nextHitTime = _animator.GetCurrentAnimatorClipInfo(0).Length + .8f;
                    Debug.Log($"{nextHitTime} our hit time");
                }
                else if (hitCount == 1 && canAttack)
                {
                    ind = Random.Range(0, middleAttacksOH.Length);
                    _animator.CrossFade(middleAttacksOH[ind], .3f);
                    nextHitTime = _animator.GetCurrentAnimatorClipInfo(0).Length + .8f;
                }
                else if (hitCount == 2 && canAttack)
                {
                    Random.Range(0, finisherAttacksOH.Length);
                    _animator.CrossFade(finisherAttacksOH[ind], .3f);
                    nextHitTime = _animator.GetCurrentAnimatorClipInfo(0).Length + .8f;
                }
            }
            else if (busyTH)
            {
                if (hitCount == 0 && canAttack && _speed > 2.0f)
                {
                    _animator.Play("slide");
                    nextHitTime = _animator.GetCurrentAnimatorClipInfo(0).Length + .8f;
                }
                else if ((hitCount == 0 || hitCount == 1) && canAttack)
                {
                    ind = Random.Range(0, startAttacksTH.Length);
                    _animator.Play(startAttacksTH[ind]);
                    nextHitTime = _animator.GetCurrentAnimatorClipInfo(0).Length + .8f;
                }
                else if (hitCount == 2 && canAttack)
                {
                    ind = Random.Range(0, middleAttacksTH.Length);
                    _animator.CrossFade(middleAttacksTH[ind], .3f);
                    nextHitTime = _animator.GetCurrentAnimatorClipInfo(0).Length + .8f;
                }
                else if (hitCount == 3 && canAttack)
                {
                    Random.Range(0, finisherAttacksTH.Length);
                    _animator.CrossFade(finisherAttacksTH[ind], .3f);
                    nextHitTime = _animator.GetCurrentAnimatorClipInfo(0).Length + .5f;
                }
            }
            


            _animator.applyRootMotion = true;
            
        }

        public void StopHit()
        {
            _animator.applyRootMotion = false;
            //_animator.SetBool(_animIDHit, false);
        }

        public void GroundedCheck()
        {
            RaycastHit hit;
            // set sphere position, with offset
            Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - GroundedOffset,
                transform.position.z);
            Grounded = Physics.CheckSphere(spherePosition, GroundedRadius, GroundLayers,
                QueryTriggerInteraction.Ignore) || Physics.Raycast(spherePosition, -transform.up, 3 * GroundedRadius, GroundLayers, QueryTriggerInteraction.Ignore);

            if (TerrainChecker.instance)
                TerrainChecker.instance.ChangeFootsteps(transform.position);

            if (_hasAnimator)
            {
                _animator.SetBool(_animIDGrounded, Grounded);
            }
        }

        public void JumpAndGravity()
        {
            if (Grounded)
            {
                // reset the fall timeout timer
                _fallTimeoutDelta = FallTimeout;

                // update animator if using character
                if (_hasAnimator)
                {
                    //animator.SetBool(_animIDJump, false);
                    _animator.SetBool(_animIDFreeFall, false);
                }

                // stop our velocity dropping infinitely when grounded
                if (_verticalVelocity < 0.0f)
                {
                    _verticalVelocity = -2f;
                }

                // jump timeout
                if (_jumpTimeoutDelta >= 0.0f)
                {
                    _jumpTimeoutDelta -= Time.deltaTime;
                }
            }
            else
            {
                // reset the jump timeout timer
                _jumpTimeoutDelta = JumpTimeout;

                // fall timeout
                if (_fallTimeoutDelta >= 0.0f)
                {
                    _fallTimeoutDelta -= Time.deltaTime;
                }
                else
                {
                    //Debug.Log("Set");
                    // update animator if using character
                    if (_hasAnimator)
                    {
                        _animator.SetBool(_animIDFreeFall, true);
                    }
                }

                // if we are not grounded, do not jump
                _input.jump = false;
            }

            // apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
            if (_verticalVelocity < _terminalVelocity)
            {
                _verticalVelocity += Gravity * Time.deltaTime;
            }
        }

        public static float ClampAngle(float lfAngle, float lfMin, float lfMax)
        {
            if (lfAngle < -360f) lfAngle += 360f;
            if (lfAngle > 360f) lfAngle -= 360f;
            return Mathf.Clamp(lfAngle, lfMin, lfMax);
        }

        public void OnDrawGizmosSelected()
        {
            Color transparentGreen = new Color(0.0f, 1.0f, 0.0f, 0.35f);
            Color transparentRed = new Color(1.0f, 0.0f, 0.0f, 0.35f);

            if (Grounded) Gizmos.color = transparentGreen;
            else Gizmos.color = transparentRed;

            // when selected, draw a gizmo in the position of, and matching radius of, the grounded collider
            Gizmos.DrawSphere(
                new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z),
                GroundedRadius);
            Gizmos.DrawLine(transform.position, transform.position - transform.up * 3 * GroundedRadius);

            if (canRoll) Gizmos.color = Color.blue;
            else Gizmos.color = Color.red;

        }

        /*private void OnFootstep(AnimationEvent animationEvent)
        {
            if (animationEvent.animatorClipInfo.weight > 0.5f)
            {
                if (FootstepAudioClips.Length > 0)
                {
                    var index = Random.Range(0, FootstepAudioClips.Length);
                    AudioSource.PlayClipAtPoint(FootstepAudioClips[index], transform.TransformPoint(_controller.center), FootstepAudioVolume);
                }
            }
        }

        private void OnLand(AnimationEvent animationEvent)
        {
            if (animationEvent.animatorClipInfo.weight > 0.5f)
            {
                AudioSource.PlayClipAtPoint(LandingAudioClip, transform.TransformPoint(_controller.center), FootstepAudioVolume);
            }
        }*/
        // Footsteps
        public void PlayerFootsteps()
        {
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(Audiomanager.instance.footsteps, transform);
            Audiomanager.instance.PlayerFootsteps();
        }

        public void PlayerRoll()
        {
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(Audiomanager.instance.roll, transform);
            Audiomanager.instance.PlayerRoll();
        }
    }
}