using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.VirtualTexturing;

public class EquipmentSystem : MonoBehaviour
{
    public static EquipmentSystem Instance;

    [Header("Weapons")]
    public GameObject weaponHolder;
    public ItemBase weaponOH;
    public ItemBase weaponTH;
    public GameObject weaponSheathOH;
    public GameObject weaponSheathTH;
    public GameObject currentWeaponInHand = null;
    GameObject currentWeaponInSheathOH;
    GameObject currentWeaponInSheathTH;

    [Header("Buffs")]
    //public Transform buffPlace;
    public Transform buffHolder;
    public ItemBase currentBuff;
    //public GameObject buffOnBody;
    public GameObject buffInHands;

    private void Start()
    {
        if (Instance == null)
            Instance = this;

        currentWeaponInSheathOH = null; currentWeaponInHand = null;
        currentWeaponInHand = null;
       /* currentWeaponInSheathOH = Instantiate(weaponOH.Model, weaponSheathOH.transform);
        currentWeaponInSheathTH = Instantiate(weaponTH.Model, weaponSheathTH.transform);*/
    }


    public void PutBuffInHand()
    {
        buffInHands = Instantiate(currentBuff.Model, buffHolder);
    }

    public void DestroyBuffInHand()
    {
        Debug.Log(buffInHands);
        Destroy(buffInHands);
    }

    public void EquipWeapon(ItemBase newWeapon)
    {
        Destroy(currentWeaponInSheathOH);
        Destroy(currentWeaponInSheathTH);
        // Debug.Log("Equipped: " + newWeapon);
        if (newWeapon.weaponType == WeaponType.OneHand)
        {
            weaponOH = newWeapon;
            Debug.Log(weaponOH);
            currentWeaponInSheathOH = Instantiate(weaponOH.Model, weaponSheathOH.transform);
        }
       else if (newWeapon.weaponType == WeaponType.TwoHand)
        {
            weaponOH = newWeapon;
            Debug.Log(weaponTH);
            currentWeaponInSheathTH = Instantiate(weaponTH.Model, weaponSheathTH.transform);
        }

    }

    public void UneqipWeapon()
    {
        Destroy(currentWeaponInSheathOH);
        Destroy(currentWeaponInSheathTH);
    }

    public void EquipWeaponHand()
    {
        Instantiate(currentWeaponInHand, weaponHolder.transform);
    }

    public void DrawWeaponOH()
    {
        //Debug.Log("Draw");
        currentWeaponInHand = Instantiate(weaponOH.Model, weaponHolder.transform);
        Destroy(currentWeaponInSheathOH);
    }

    public void SheathWeaponOH()
    {
       //Debug.Log("Sheath");
        currentWeaponInSheathOH = Instantiate(weaponOH.Model, weaponSheathOH.transform);
        Destroy(currentWeaponInHand);
    }

    public void DrawWeaponTH()
    {
        Debug.Log("DrawTH");
        currentWeaponInHand = Instantiate(weaponTH.Model, weaponHolder.transform);
        Destroy(currentWeaponInSheathTH);
    }

    public void SheathWeaponTH()
    {
        Debug.Log("SheathTH");
        currentWeaponInSheathTH = Instantiate(weaponTH.Model, weaponSheathTH.transform);
        Destroy(currentWeaponInHand);
    }

    public void StartDealDamage()
    {
        Debug.Log("Start Dealilng Damage");
        if (currentWeaponInHand)
        {
            currentWeaponInHand.GetComponent<DamageDealer>().StartDealDamage();
        }
    }

    public void EndDealDamage()
    {
        Debug.Log("End Dealing Damage");
        if (currentWeaponInHand)
        {
            currentWeaponInHand.GetComponent<DamageDealer>().EndDealDamage();
        }
    }
}
