using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatistics : MonoBehaviour
{
    [SerializeField] PlayerStats stats;
    [SerializeField] UI_lean_tween ui_stats;
    private int _animIDDamage;
    private int _animIDDeath;
    private Animator animator;
    private ThirdPersonController playerController;


    // Start is called before the first frame update
    void Start()
    {
        AssignAnimatorIDs();
        animator = GetComponent<Animator>();
        playerController = GetComponent<ThirdPersonController>();   
    }

    private void Death()
    {
        playerController.dead = true;
        animator.SetTrigger(_animIDDeath);
        Debug.Log("Death");
    }

    public void TakeDamage(float damageAmount)
    {
        Debug.Log("Got an arrow");
        ui_stats.Add_hp(-damageAmount);
        

        if (!playerController.dead)
        {
            if (stats.health <= 0) Death();
            else animator.SetTrigger(_animIDDamage);
        }
    }

    private void AssignAnimatorIDs()
    {
        _animIDDamage = Animator.StringToHash("Damage");
        _animIDDeath = Animator.StringToHash("Death");
    }
}
