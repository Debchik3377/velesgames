using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu(menuName = "Stats/Player")]
public class PlayerStats : ScriptableObject
{
    public float health = 120f;
    public float max_health = 120f;
    public int hearts_count = 0;
    public Vector3 position;
    public float damageMultiplier = 1;
    public float stamina = 100f;
    public float max_stamina = 100f;

    //������ ��������
    public int damage_lvl = 0;
    public int health_lvl = 0;
    public int max_dm_lvl = 5;
    public int max_hp_lvl = 5;

    public int game_level_index = 1;//������� ����� � �������

    //�� �����, �� ��������(����� 12 ����)
    public float current_hp_wight = 300;

    public float staminaRecover;

    private float lerpSpeed = 0.1f;
    public void SpendStamina(float value)
    {
        Debug.Log($"Spent {value} stamina");
        float targetStamina = stamina - value;
        stamina = targetStamina;
        //stamina = Mathf.SmoothDamp(stamina, targetStamina, ref velocity, Time.deltaTime);
    }

}
