using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.UI;
using StarterAssets;

public class PickableItem : MonoBehaviour
{
    [SerializeField] public ItemBase item;

    [SerializeField] private GameObject button;
    [SerializeField] private TMP_Text text;
    [SerializeField] private StarterAssetsInputs inputs;
    [SerializeField] private InventoryManager inventoryManager;
    [SerializeField] private Outline outline;

    private void Start()
    {
        inventoryManager = InventoryManager.instance;
        inputs = StarterAssetsInputs.Instance;
        outline = GetComponent<Outline>();
        inventoryManager = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Contains("Player"))
        {
            outline.enabled = true;
            text.text = "Press E to pick up" + item.name;
            button.SetActive(true);
            Debug.Log(button.ToString());
            Debug.Log(other.gameObject.name);
            inputs = other.gameObject.GetComponent<StarterAssetsInputs>();
            //Debug.Log(inputs.ToString());
        }
        

    }

    private void OnTriggerStay(Collider other)
    {

        if (other.gameObject.tag.Contains("Player") && inputs.pickUp)
        {
            inputs.pickUp = false;
            if (inventoryManager.AddItem(item))
            {
                button.SetActive(false);
                Destroy(gameObject);
            }
            else
            {
                text.text = "Cannot pick up it";
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Contains("Player"))
        {
            outline.enabled = false;
            text.text = "Press E to pick up item";
            button.SetActive(false);
        }
           
    }
}
