using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class WeaponSlot : InventorySlot, IDropHandler
{
    public EquipmentSystem equipmentSystem;
    private void Awake()
    {
        equipmentSystem = GameObject.Find("Player").GetComponent<EquipmentSystem>(); 
        playerController = GameObject.Find("Player").GetComponent<ThirdPersonController>(); 
    }

    public override void OnDrop(PointerEventData eventData)
    {
        InventoryItem dragInventoryItem = eventData.pointerDrag.GetComponent<InventoryItem>();
        if (dragInventoryItem.item.type == ItemType.Weapon)
        {
            PlayerCombat.Instance.comboStand = dragInventoryItem.item.combo;
            PlayerCombat.Instance.comboMove = dragInventoryItem.item.comboMove;
            if (!playerController.busyOH && !playerController.busyTH)
            {
                if (transform.childCount == 0)
                {
                    dragInventoryItem.parentAfterDrag = transform;
                }
                else
                {
                    transform.GetChild(0).SetParent(dragInventoryItem.parentAfterDrag);
                    dragInventoryItem.parentAfterDrag = transform;
                }

                if (dragInventoryItem.item.weaponType == WeaponType.OneHand)
                {
                    equipmentSystem.EquipWeapon(dragInventoryItem.item);
                }
                else if (dragInventoryItem.item.weaponType == WeaponType.TwoHand)
                {
                    equipmentSystem.EquipWeapon(dragInventoryItem.item);
                }
            }
            else
            {
                transform.GetChild(0).SetParent(dragInventoryItem.parentAfterDrag);
                dragInventoryItem.parentAfterDrag = transform;
                if (dragInventoryItem.item.weaponType == WeaponType.OneHand)
                {
                    playerController.SetBusyOH(true);
                    playerController.SetBusyTH(false);
                }
                else if (dragInventoryItem.item.weaponType == WeaponType.TwoHand) 
                {
                    playerController.SetBusyOH(false);
                    playerController.SetBusyTH(true);
                }
                playerController.PlayChanging();
                playerController.newWeaponItem = dragInventoryItem.item.Model;
                Debug.Log(dragInventoryItem.item.Model);
            }
            
        }
    }
}
