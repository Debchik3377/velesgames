using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

public class Locomotion : StateMachineBehaviour
{
    [SerializeField] private StarterAssetsInputs _inputs;

    [SerializeField] private EquipmentSystem _equipmentSystem;

    [SerializeField] private float recoverStaminaValue;
    [SerializeField] private float recoverStaminaMultiplier;

    private int frameCount = 0;

    private void Awake()
    {
        GameObject player = GameObject.Find("Player");
        _inputs = player.GetComponent<StarterAssetsInputs>();
        playerController =  player.GetComponent<ThirdPersonController>();
    }

    //private Animator _animator;
    [SerializeField] private ThirdPersonController playerController;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        playerController.canAttack = true;
        _inputs.roll = false;
        _inputs.hit = false;
        _inputs.equip = false;

       /* playerController.recoverStaminaValue = recoverStaminaValue;
        playerController.recoverStaminaMultiplier = recoverStaminaMultiplier;*/

        //Debug.Log(playerController);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        frameCount++;

        playerController.Move();

        if (_inputs.roll && !playerController.inAction && !playerController._inventory)
        {
            Debug.Log("Rooll");
            playerController.delta = 0;
            playerController.CallRoll();
        }

        if (_inputs.hit && (playerController.busyOH || playerController.busyTH) && !playerController._inventory && playerController.playerStats.stamina > 0)
        {
            PlayerCombat.Instance.Attack();
            _inputs.hit = false;
        }

        if (_inputs.equip && !playerController.attacking)
        {
            playerController.PutOutInItem();
        }

        _inputs.roll = false;
        _inputs.hit = false;
        _inputs.equip = false;
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //playerController._controller.height = playerController.fallHeight;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
       /* Vector3 velocity = animator.deltaPosition;
        ThirdPersonController.instance._controller.Move(velocity);*/
    }

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}


}
