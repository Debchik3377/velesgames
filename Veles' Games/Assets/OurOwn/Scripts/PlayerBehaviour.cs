using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using StarterAssets;
using System.Diagnostics.Tracing;
//using UnityEditor.Animations;

public class PlayerBehaviour : MonoBehaviour
{
    public States playerState;
    public ThirdPersonController _playerController;
    public StarterAssetsInputs _input;

    private CharacterController _controller;
    private Animator _animator;


    //Animations
    private int _animIDDrawWeapon_OH;
    private int _animIDSheathWeapon_OH;
    private int _animIDHit;
    private int _animIDRoll;

    //Timeouts
    private float _inActionTime;
    private float _currentActionTime;

    //States
    public bool _busyOH = false;
    public bool _busyTH = false;
    public bool _inAction = false;
    public bool _lockedon = false;
    public bool _inventory = false;

    //Cinemachine
    public CinemachineVirtualCamera playerFollowCamera;
    public CinemachineVirtualCamera lockonCamera;
    public GameObject targetLockon;
    public GameObject targetDefault;

    //UI
    public GameObject inventoryPlane;

    private Vector3 boxSixe = new Vector3(5, 10, 50);

    private void Awake()
    {
        AssignAnimationIDs();
    }

    // Start is called before the first frame update
    void Start()
    {
        _controller = GetComponent<CharacterController>();
        _playerController= GetComponent<ThirdPersonController>();
        _input = GetComponent<StarterAssetsInputs>();
        _animator = GetComponent<Animator>();
        _inActionTime = 0.5f;
        _currentActionTime = _inActionTime;
        _initHeight = _controller.height;
    }

    // Update is called once per frame
    void Update()
    {
        if (_inventory)
            return;
        
        if (_currentActionTime < _inActionTime) _currentActionTime += Time.deltaTime;
        _inAction = _currentActionTime < _inActionTime;
       // _playerController.inAction = _inAction;

        if (playerState == States.Standing)
        {
            _playerController.Move();
            _playerController.GroundedCheck();
            _playerController.JumpAndGravity();
            Actions();
        }
        else if (playerState == States.Running)
        {
            _playerController.Move();
            _playerController.GroundedCheck();
            _playerController.JumpAndGravity();
            Actions();
        }
        else if (playerState == States.Jumping)
        {
            _playerController.Move();
            _playerController.GroundedCheck();
            _playerController.JumpAndGravity();
            Actions();
        }
        else if (playerState == States.Rolling)
        {
            _playerController.ApplyRootMotion();
        }
        else if (playerState == States.Action)
        {

        }
        
    }

    public void OpenCloseInventory()
    {
        _inventory = !_inventory;
        inventoryPlane.SetActive(!inventoryPlane.activeInHierarchy);
        _input.cursorLocked = !_inventory;
        _input.SetCursorState(!_inventory);
        Debug.Log(!_inventory);
    }

    private void FixedUpdate()
    {
        _input.jump = false;
        _input.roll = false;
        _input.equip = false;
        _animator.SetBool(_animIDHit, false);
    }

    private void AssignAnimationIDs()
    {
        _animIDDrawWeapon_OH = Animator.StringToHash("DrawWeaponOH");
        _animIDSheathWeapon_OH = Animator.StringToHash("SheathWeaponOH");
        _animIDHit = Animator.StringToHash("hit");
        _animIDRoll = Animator.StringToHash("Roll");
    }

    private void Actions()
    {
        /*if (_inAction)
            return;*/

        if (_input.equip)
        {
            _currentActionTime = 0;
            _input.equip = false;
            _inActionTime = 1.5f;
            if (_busyOH)
            {
                _animator.SetTrigger(_animIDSheathWeapon_OH);
            }
            else
            {
                _animator.SetTrigger(_animIDDrawWeapon_OH);
            }
            _busyOH = !_busyOH;
        }
        if (_playerController.Grounded && _input.roll)
        {
            ChangeState(States.Action);
            if (_playerController._speed < 2f)
            {
                _animator.SetFloat("Speed", 0f);
            }
            _currentActionTime = 0;
            _animator.applyRootMotion = true;
            rolling = true;
            ChangeState(States.Rolling);
            _animator.SetTrigger(_animIDRoll);
        }
        
        _input.roll = false;
    }

    public void SetTarget()
    {
        _lockedon = true;
        playerFollowCamera.Priority = 5;
        lockonCamera.Priority = 10;
    }

    public void RemoveTarget()
    {
        _lockedon = false;
        playerFollowCamera.Priority = 10;
        lockonCamera.Priority = 5;
    }

    public void Hit()
    {
        ChangeState(States.Action);
        _animator.SetBool(_animIDHit, true);
    }

    public void EndRoll()
    {
        rolling = false;
        SetHeight(_initHeight);
    }

    public void EndJump()
    {
        Debug.Log("EndJump");
        SetHeight(_initHeight);
    }

    public float _initHeight;
    public float _jumpHeight;
    public float _rollHeight;
    public float _crouchHeight;

    public bool rolling;

    public void ChangeState(States nextState)
    {
        playerState = nextState;
        if (nextState == States.Standing)
        {
            //Debug.Log("Standing");
            //animatorOverrider.HandMaskOff();
            _animator.applyRootMotion = false;
            _controller.height = _initHeight;
        }
        else if (nextState == States.Running)
        {
            //Debug.Log("Running");
            //animatorOverrider.HandMaskOn();
            _animator.applyRootMotion = false;
            _controller.height = _initHeight;
        }
        else if (nextState == States.Jumping)
        {
            //Debug.Log("Jumping");
            _animator.applyRootMotion = false;
           // animatorOverrider.HandMaskOn();
            //_controller.height = _jumpHeight;
        }
        else if (nextState == States.Rolling)
        {
            //Debug.Log("Rolling");
            _animator.applyRootMotion = true;
            _controller.height = _rollHeight;
        }
        else if (nextState == States.Crouching)
        {
            //Debug.Log("Crouching");
            _animator.applyRootMotion = false;
            //animatorOverrider.HandMaskOn();
            _controller.height = _crouchHeight;
        }
        else if (nextState == States.Action)
        {
            //Debug.Log("Standing");
            _animator.SetFloat("Speed", 0f);
            //animatorOverrider.HandMaskOff();
            _animator.applyRootMotion = false;
        }
    }

    public void SetHeight(float height)
    {
        Debug.Log(height);
        _controller.height = height;
    }
}

public enum States
{
    Standing, Action, Running, Jumping, Rolling, Crouching
}
