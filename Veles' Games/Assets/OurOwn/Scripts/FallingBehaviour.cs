using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

public class FallingBehaviour : StateMachineBehaviour
{

    [SerializeField] private ThirdPersonController playerController;

    private float[] weights;

    [SerializeField] private float recoverStaminaValue;
    [SerializeField] private float recoverStaminaMultiplier;


    private void Awake()
    {
        playerController = GameObject.Find("Player").GetComponent<ThirdPersonController>();
    }

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        playerController.recoverStaminaValue = recoverStaminaValue;
        playerController.recoverStaminaMultiplier = recoverStaminaMultiplier;

        playerController.Move();
        playerController._controller.height = playerController.standHeight;
        //Debug.Log(layerIndex + " " + animator.layerCount);
        weights = new float[animator.layerCount];
       for (int i = 1; i < animator.layerCount; ++i)
        {
            weights[i] = animator.GetLayerWeight(i);
            animator.SetLayerWeight(i, 0);
        }

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
        playerController.MoveVertically();
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        for (int i = 1; i < animator.layerCount; ++i)
        {
            animator.SetLayerWeight(i, weights[i]);
        }
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}


}
