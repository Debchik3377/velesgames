using StarterAssets;
using System.Collections;
using UnityEngine;
#if ENABLE_INPUT_SYSTEM
using UnityEngine.InputSystem;
#endif

public class StarterAssetsInputs : MonoBehaviour
{
    public static StarterAssetsInputs Instance;

    [Header("Character Input Values")]
    public Vector2 move;
    public Vector2 look;
    public bool jump;
    public bool sprint;
    public bool roll;
    public bool hit;
    public bool equip;
    public bool pickUp;

    [Header("Movement Settings")]
    public bool analogMovement;

    [Header("Mouse Cursor Settings")]
    public bool cursorLocked = true;
    public bool cursorInputForLook = true;

    public PlayerBehaviour playerBehaviour;
    public ThirdPersonController playerController;

    private void Start()
    {
        if (Instance == null)
            Instance = this;
        playerController = GetComponent<ThirdPersonController>();
        playerBehaviour = GetComponent<PlayerBehaviour>();
    }

#if ENABLE_INPUT_SYSTEM

    public void OnTakeBuff()
    {
        playerController.BuffPlayer();
            
    }

    public void OnMove(InputValue value)
    {
        MoveInput(value.Get<Vector2>());
    }

    public void OnLook(InputValue value)
    {
        if (cursorInputForLook)
        {
            LookInput(value.Get<Vector2>());
        }
    }

    public void OnJump(InputValue value)
    {
        JumpInput(value.isPressed);
    }

    public void OnSprint(InputValue value)
    {
        SprintInput(value.isPressed);
    }

    public void OnPutOut()
    {
        Equip();
    }

    public void OnLockon()
    {
        Lockon();
    }

    public void OnHit()
    {
        Hit();
    }

    public void OnRoll()
    {
        RollInput();
    }

    public void OnInventory()
    {
        Inventory();
    }

    public void OnNpsActivate()
    {
        pickUp = true;
    }


#endif
    private void Hit()
    {
        hit = true;
    }

    public void Inventory()
    {
        playerController.OpenCloseInventory();
    }

    public void MoveInput(Vector2 newMoveDirection)
    {
        move = newMoveDirection;
    }

    public void LookInput(Vector2 newLookDirection)
    {
        look = newLookDirection;
    }

    public void JumpInput(bool newJumpState)
    {
        jump = newJumpState;
    }

    public void SprintInput(bool newSprintState)
    {
        sprint = newSprintState;
    }

    public void RollInput()
    {
        roll = true;
    }

    public void Equip()
    {
        equip = true;
    }

    public void Lockon()
    {
        Debug.Log(playerBehaviour._lockedon);
        if (playerBehaviour._lockedon)
        {
            playerBehaviour.RemoveTarget();
        }
        else
        {
            playerBehaviour.SetTarget();
        }
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        SetCursorState(cursorLocked);
    }

    public void SetCursorState(bool newState)
    {
        Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None; // first - Locked

    }
}
