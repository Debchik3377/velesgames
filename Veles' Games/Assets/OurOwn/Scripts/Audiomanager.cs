using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;

public class Audiomanager : MonoBehaviour
{
    private List<EventInstance> eventinstances;
    public static Audiomanager instance;

    [Header("UI")]
    public EventReference on_click;
    public EventReference not_enough;
    public EventReference new_lvl;
    public EventReference item_drag;
    public EventReference pick_up_heart;

    [Header("Player")]
    public EventReference _footsteps;
    public EventInstance footsteps;

    public EventReference _roll;
    public EventInstance roll;

    public EventReference _breath;
    public EventInstance breath;

    public EventReference _death;

    [Header("Vii")]
    public EventReference _ViiRoar1;
    public EventInstance ViiRoar1;

    public EventReference _ViiRoar2;
    public EventInstance ViiRoar2;

    public EventReference _ViiFootsteps;
    public EventInstance ViiFootsteps;

    public EventReference _ViiBreath;
    public EventInstance ViiBreath;

    public EventReference _ViiAgony;
    public EventInstance ViiAgony;

    public EventReference _ViiAttackGround;
    public EventInstance ViiAttackGround;

    public EventReference _ViiLand;
    public EventInstance ViiLand;

    public EventReference _ViiGetHit;
    public EventInstance ViiGetHit;

    public EventReference _ViiAttack;
    public EventInstance ViiAttack;

    public EventReference _wistle;

    public EventReference _sword_in;

    public EventReference _sword_out;

    public EventReference _sword_miss;

    public EventReference _take_dmg_melee;

    public EventReference _take_dmg_arrow;


    public void PlayOnClickEvent() { RuntimeManager.PlayOneShot(on_click); }
    public void PlayNotEnoughEvent() { RuntimeManager.PlayOneShot(not_enough); }
    public void PlayNewLvlEvent() { RuntimeManager.PlayOneShot(new_lvl); }
    public void PlayItemDragEvent() { RuntimeManager.PlayOneShot(item_drag); }
    public void PlayPickUpHeart() { RuntimeManager.PlayOneShot(pick_up_heart); }
    public void PlayWistle() { RuntimeManager.PlayOneShot(_wistle); }
    public void PlaySword_out() { RuntimeManager.PlayOneShot(_sword_out); }
    public void PlaySword_in() { RuntimeManager.PlayOneShot(_sword_in); }
    public void PlaySword_miss() { RuntimeManager.PlayOneShot(_sword_miss); }
    public void PlayTake_dmg_melee() { RuntimeManager.PlayOneShot(_take_dmg_melee); }
    public void PlayTake_dmg_arrow() { RuntimeManager.PlayOneShot(_take_dmg_arrow); }
    public void PlayDeath() { RuntimeManager.PlayOneShot(_death); breath.stop(FMOD.Studio.STOP_MODE.IMMEDIATE); }


    public EventInstance CreateInstance(EventReference eventref)
    {
        EventInstance eventInstance = RuntimeManager.CreateInstance(eventref);
        eventinstances.Add(eventInstance);
        return eventInstance;
    }

    public void PlayerFootsteps() { if (footsteps.isValid()) footsteps.start(); }
    public void PlayerRoll() { if (roll.isValid()) roll.start(); }
    public void PlayViiRoar1() { if (ViiRoar1.isValid()) ViiRoar1.start(); }
    public void PlayViiRoar2() { if (ViiRoar2.isValid()) ViiRoar2.start(); }
    public void PlayViiFootsteps() { if (ViiFootsteps.isValid()) ViiFootsteps.start(); }
    public void PlayViiGetHit() { if (ViiGetHit.isValid()) ViiGetHit.start(); }
    public void PlayViiBreath() { if (ViiBreath.isValid()) ViiBreath.start(); }
    public void PlayViiAgony() { if (ViiAgony.isValid()) ViiAgony.start(); ViiBreath.stop(FMOD.Studio.STOP_MODE.IMMEDIATE); }
    public void PlayViiAttack() { if (ViiAttack.isValid()) ViiAttack.start(); }
    public void PlayViiLand() { if (ViiLand.isValid()) ViiLand.start(); }
    public void PlayViiAttackGround() { if (ViiAttackGround.isValid()) ViiAttackGround.start(); }


    private void Awake()
    {
        if (instance != null) Debug.LogError("Больше одного Audiomanager в сцене");
        instance = this;

        eventinstances = new List<EventInstance>();

        footsteps = CreateInstance(_footsteps);
        roll = CreateInstance(_roll);
        breath = CreateInstance(_breath);
        ViiRoar1 = CreateInstance(_ViiRoar1);
        ViiRoar2 = CreateInstance(_ViiRoar2);
        ViiFootsteps = CreateInstance(_ViiFootsteps);
        ViiBreath = CreateInstance(_ViiBreath);
        ViiAgony = CreateInstance(_ViiAgony);
    }

    private void CleanUp()
    {
        foreach (EventInstance eventInstance in eventinstances)
        {
            eventInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            eventInstance.release();
        }
    }

    private void OnDestroy()
    {
        CleanUp();
    }
}
