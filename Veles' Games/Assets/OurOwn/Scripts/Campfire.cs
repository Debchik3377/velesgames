using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Campfire : MonoBehaviour
{


    private void OnTriggerEnter(Collider other)
    {
        ThirdPersonController playerController = other.GetComponent<ThirdPersonController>();
        if (playerController)
        {
            playerController.SetCheckpoint(transform.position + transform.forward);
        }
    }
}
