using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

public class Hit : StateMachineBehaviour
{
    [SerializeField] private ThirdPersonController playerController;
    [SerializeField] private StarterAssetsInputs _inputs;

    [SerializeField] private float recoverStaminaValue;
    [SerializeField] private float recoverStaminaMultiplier;

    private UI_lean_tween uI;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GameObject player = GameObject.Find("Player");
        _inputs = player.GetComponent<StarterAssetsInputs>();
        playerController = player.GetComponent<ThirdPersonController>();

        //playerController._speed = 0;
        //animator.SetFloat("Speed", 0, 0.01f, Time.deltaTime);

        playerController.recoverStaminaValue = recoverStaminaValue;
        playerController.recoverStaminaMultiplier = recoverStaminaMultiplier;
        uI = GameObject.Find("UIStatsManager").GetComponent<UI_lean_tween>();
        uI.Set_st(playerController.playerStats.stamina);
        //Debug.Log(playerController.playerStats.stamina);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        /*//playerController.ApplyRootMotion();
        if (_inputs.hit && playerController.canAttack && (playerController.busyOH || playerController.busyTH) && !playerController._inventory && playerController.playerStats.stamina > 0)
        {
            playerController.delta = 0;
            playerController.StartHit();
        }*/
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.applyRootMotion = false;
        playerController.inAction = false;
        playerController.canRotate = true;
        //ThirdPersonController.instance.isAttacking = false;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //playerController._controller.Move(animator.deltaPosition);
    }

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
