using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class scene_manager : MonoBehaviour
{
    public PlayerStats playerstats;
    public GameObject load_screen;
    public Slider bar;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void Next_scene()
    {
        playerstats.game_level_index++;
        StartCoroutine(loadAsync(playerstats.game_level_index));
    }
    public void Set_scene(int index)
    {
        playerstats.game_level_index = index;
        StartCoroutine(loadAsync(playerstats.game_level_index));
    }
    public void Exit_game()
    {
        Debug.Log("end");
        SceneManager.LoadScene(0);
        Application.Quit();
    }

    IEnumerator loadAsync(int index)
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(index);

        while (!async.isDone)
        {
            bar.value = async.progress;
            yield return null;
        }
    }
}
