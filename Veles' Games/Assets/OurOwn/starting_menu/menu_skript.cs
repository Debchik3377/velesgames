using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class menu_skript : MonoBehaviour
{
    public PlayerStats playerstats;
    public TextMeshProUGUI text;
    public GameObject load_screen;
    public Slider bar;

    void Awake()
    {
        if (playerstats.game_level_index == 1) text.text = "������ ����";
        else text.text = "���������� ����";
    }
    public void Start_game()
    {
        load_screen.SetActive(true);
        StartCoroutine(loadAsync(playerstats.game_level_index));
    }

    IEnumerator loadAsync(int index)
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(index);
        while (!async.isDone)
        {
            bar.value = async.progress;
            yield return null;
        }
    }
    public void Exit_game()
    {
        SceneManager.LoadScene(0);
        Application.Quit();
    }
}
