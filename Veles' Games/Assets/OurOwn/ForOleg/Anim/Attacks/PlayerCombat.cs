using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

public class PlayerCombat : MonoBehaviour
{
    public static PlayerCombat Instance;

    public List<AttacksSO> comboStand;
    public List<AttacksSO> comboMove;
    private float lastClickedTime;
    private float lastComboEnd;

    private int speedComboCounter;
    private int comboCounter;

    private Animator animator;
    private ThirdPersonController player;
    private static int _animIDAttack;
    private static int _animIDSpeed;

    public float nextHitOffset = 0.3f;
    public float SmoothTime = 0.3f;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _animIDAttack = Animator.StringToHash("Attack");
        _animIDSpeed = Animator.StringToHash("Speed");
        animator = ThirdPersonController.instance.GetComponent<Animator>();
        player = GetComponent<ThirdPersonController>();
    }

    // Update is called once per frame
    void Update()
    {
        ExitAttack();
    }

    public void Attack()
    {
        if (player.inAction)
            return;
        if (Time.time - lastComboEnd > 0.5f && comboCounter <= comboStand.Count && player._speed <= player.MoveSpeed)
        {
            CancelInvoke("EndCombo");

            if (animator.GetCurrentAnimatorStateInfo(1).IsTag("Attack") && animator.GetCurrentAnimatorStateInfo(1).normalizedTime >= 1f || !animator.GetCurrentAnimatorStateInfo(1).IsTag("Attack") && Time.time - lastClickedTime >= 0.5f)
            {
                StandAttackCheck();
            }
        }
        else if (Time.time - lastComboEnd > 0.5f && speedComboCounter <= comboMove.Count && player._speed > player.MoveSpeed)
        {
            CancelInvoke("EndCombo");

            if (animator.GetCurrentAnimatorStateInfo(1).IsTag("Attack") && animator.GetCurrentAnimatorStateInfo(1).normalizedTime >= 1f || !animator.GetCurrentAnimatorStateInfo(1).IsTag("Attack") && Time.time - lastClickedTime >= 0.5f)
            {
                SpeedAttackCheck();
            }
        }
    }

    private void SpeedAttackCheck()
    {
        if (speedComboCounter >= comboMove.Count - 1)
        {
            speedComboCounter = 0;
        }

        //Stamina NIKITA
        if (!player.Untiring && comboMove[speedComboCounter].staminaCost + player.currentWeapon.Efforts > player.playerStats.stamina)
            return;
        if (!player.Untiring)
            player.playerStats.SpendStamina(comboMove[speedComboCounter].staminaCost + player.currentWeapon.Efforts);

        animator.runtimeAnimatorController = comboMove[speedComboCounter].animatorOV;
        animator.CrossFade(_animIDAttack, 0.4f, 1, 0, 0);
        player.canRotate = false;

        lastClickedTime = Time.time;

        AddSpeedComboCount();
    }

    private void StandAttackCheck()
    {
        player.inAction = true;
        ThirdPersonController.instance._speed = 0f;
        animator.SetFloat(_animIDSpeed, 0f);
        //animator.applyRootMotion = true;

        //Stamina NIKITA
        if (comboCounter >= comboStand.Count - 1)
        {
            comboCounter = 0;
        }
        if (!player.Untiring && comboStand[comboCounter].staminaCost + player.currentWeapon.Efforts > player.playerStats.stamina)
            return;

        // For equipment system damage

        animator.runtimeAnimatorController = comboStand[comboCounter].animatorOV;
        animator.CrossFade(_animIDAttack, 0.4f, 1, 0, 0);
        if (!player.Untiring)
            player.playerStats.SpendStamina(comboStand[comboCounter].staminaCost + player.currentWeapon.Efforts);

        lastClickedTime = Time.time;

        AddComboCount();
    }

    private void AddSpeedComboCount()
    {
        speedComboCounter++;

        if (speedComboCounter >= comboMove.Count - 1)
        {
            speedComboCounter = 0;
        }
    }

    private void AddComboCount()
    {
        comboCounter++;

        if (comboCounter >= comboStand.Count - 1)
        {
            comboCounter = 0;
        }
    }

    public void ExitAttack()
    {
        float time = animator.GetCurrentAnimatorStateInfo(1).normalizedTime;
        if (time > 0.9f && animator.GetCurrentAnimatorStateInfo(1).IsTag("Attacks"))
        {
            Invoke("EndCombo", 1);
        }
    }

    public void EndCombo()
    {
        comboCounter = 0;
        lastComboEnd = Time.time;
    }
}
