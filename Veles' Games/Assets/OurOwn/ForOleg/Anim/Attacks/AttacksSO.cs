using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Attacks/NormalAttack")]
public class AttacksSO : ScriptableObject
{
    public AnimatorOverrideController animatorOV;
    public float damageAdd;
    public float staminaCost;
}
