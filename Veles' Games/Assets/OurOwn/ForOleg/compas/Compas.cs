using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Compas : MonoBehaviour
{
    public RectTransform compass;
    public GameObject playerCamera;

    private Vector3 previousCameraRotation;

    void Start()
    {
        previousCameraRotation = playerCamera.transform.eulerAngles;
    }

    void Update()
    {
        Vector3 cameraRotationDelta = playerCamera.transform.eulerAngles - previousCameraRotation;

        compass.Rotate(0, 0, -cameraRotationDelta.y);

        previousCameraRotation = playerCamera.transform.eulerAngles;
}
}
