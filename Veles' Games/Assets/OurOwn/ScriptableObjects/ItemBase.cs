using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(menuName = "Items/Item")]
public class ItemBase : ScriptableObject
{
    [Header("Only gameplay")]
    public TileBase tile;
    public GameObject Model;
    public ItemType type;
    public ActionType actionType;
    public Vector2Int range = new Vector2Int(7, 3);

    [Header("Only UI")]
    public bool stackable = true;
    public int maxCount = 0;

    [Header("Both")]
    public Sprite image;


    [SerializeField] private float damage;
    public float Damage { get { return damage; } }

    [SerializeField] private float efforts;
    public float Efforts { get { return efforts; } }

    [SerializeField] public WeaponType weaponType;

    public List<AttacksSO> combo;
    public List<AttacksSO> comboMove;
}

public enum ItemType
{
    Weapon,
    Potion,
    Venom,
    Armour,
}

public enum ActionType
{
    Fight,
    Defend,
    Drink,
    Throw,
}
public enum WeaponType
{
    OneHand, TwoHand, Bow
}

