using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ActivateCutscene : MonoBehaviour
{
    public float cutsceneLength;
    public bool isReplayable;

    private bool isPlayed = false;

    [SerializeField]
    UnityEvent onCutsceneActive;
    [SerializeField]
    UnityEvent onCutsceneExit;


    private void OnTriggerEnter(Collider other)
    {
        if (isPlayed == true)
            return;
        if (!isReplayable && isPlayed == false)
            isPlayed = true;
        if ( other.gameObject.tag.Contains("Player"))
        {
            Debug.Log("JDKJSLFJK");
            onCutsceneActive.Invoke();
            Invoke("Endcutscene", cutsceneLength);
        }
        
    }

    private void Endcutscene()
    {
        onCutsceneExit.Invoke();
    }
}
