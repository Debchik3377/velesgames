using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UI_lean_tween : MonoBehaviour
{
    public PlayerStats playerStats;

    public GameObject hp_bar;
    public GameObject stamina_bar;
    public TextMeshProUGUI hearts_counter;

    public RectTransform hp_rect;
    public RectTransform st_rect;

    public float starting_hp_wight;

    public AnimationCurve animationCurve;

    void Start()
    {
        hp_rect.sizeDelta = new Vector2(playerStats.current_hp_wight, hp_rect.rect.height);
        LeanTween.scaleX(hp_bar, playerStats.health / playerStats.max_health, 0.01f);
        LeanTween.scaleX(stamina_bar, playerStats.stamina / playerStats.max_stamina, 0.01f);
        Refreash_hearts();
    }

    public void Add_hp(float n)
    {
        LeanTween.cancel(hp_bar);
        playerStats.health += n;
        LeanTween.scaleX(hp_bar, playerStats.health / playerStats.max_health, 1).setEase(animationCurve);
    }

    public void Set_new_max_hp(int n)
    {
        playerStats.current_hp_wight = starting_hp_wight * (100 + n) / 100;
        hp_rect.sizeDelta = new Vector2(playerStats.current_hp_wight, hp_rect.rect.height);
        Debug.Log(hp_rect.rect.width);
    }

    public void Set_hp(float n)
    {
        LeanTween.cancel(hp_bar);
        playerStats.health = n;
        LeanTween.scaleX(hp_bar, n / playerStats.max_health, 0);
    }

    public void Add_st(float n)
    {
        LeanTween.cancel(stamina_bar);
        playerStats.stamina += n;
        LeanTween.scaleX(stamina_bar, playerStats.stamina / playerStats.max_stamina, 2f);
        
    }

    public void Set_st(float n)
    {
        LeanTween.cancel(stamina_bar);
        playerStats.stamina = n;
        LeanTween.scaleX(stamina_bar, n / playerStats.max_stamina, 0.2f);
    }

    public void Refreash_hearts()
    {
        hearts_counter.text = playerStats.hearts_count.ToString();
    }

    public void Add_hearts(int n = 1)
    {
        playerStats.hearts_count += n;
        Refreash_hearts();
    }
}
