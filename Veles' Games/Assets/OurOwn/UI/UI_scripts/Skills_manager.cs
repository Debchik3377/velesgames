using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Skills_manager : MonoBehaviour
{
    public PlayerStats playerStats;
    public int skill_case;
    public GameObject accept;
    public GameObject accept_im;
    public GameObject not_enough;
    public int skill_cost;
    public UI_lean_tween ui_stats;
    public GameObject damage_lvls;
    public GameObject health_lvls;


    void Start()
    {
        if (playerStats.damage_lvl != playerStats.max_dm_lvl) damage_lvls.transform.GetChild(playerStats.damage_lvl).gameObject.GetComponent<Button>().enabled = true;
        if (playerStats.health_lvl != playerStats.max_hp_lvl) health_lvls.transform.GetChild(playerStats.health_lvl).gameObject.GetComponent<Button>().enabled = true;
    }
    public void Next_dmg_lvl()
    {
        playerStats.damage_lvl++;
        damage_lvls.transform.GetChild(playerStats.damage_lvl - 1).gameObject.GetComponent<Button>().enabled = false;
        if (damage_lvls.transform.childCount > playerStats.damage_lvl) damage_lvls.transform.GetChild(playerStats.damage_lvl).gameObject.GetComponent<Button>().enabled = true;
    }
    public void Next_hp_lvl()
    {
        playerStats.health_lvl++;
        health_lvls.transform.GetChild(playerStats.health_lvl - 1).gameObject.GetComponent<Button>().enabled = false;
        if(health_lvls.transform.childCount > playerStats.health_lvl) health_lvls.transform.GetChild(playerStats.health_lvl).gameObject.GetComponent<Button>().enabled = true;
    }
    public bool Pay()
    {
        if (playerStats.hearts_count >= skill_cost)
        {
            ui_stats.Add_hearts(-skill_cost);
            Exit();
        }
        else
        {
            NotEnough();
        }
        return playerStats.hearts_count >= skill_cost;
    }
    public void NotEnough()
    {
        accept_im.SetActive(false);
        not_enough.SetActive(true);
    }
    public void NotEnoughExit()
    {
        accept_im.SetActive(true);
        not_enough.SetActive(false);
        accept.SetActive(false);
    }
    public void Dmg_lvl_1()
    {
        if (Pay())
        {
            playerStats.damageMultiplier += 0.01f;
            Next_dmg_lvl();
        }
    }
    public void Dmg_lvl_2()
    {
        if (Pay())
        {
            playerStats.damageMultiplier += 0.01f;
            Next_dmg_lvl();
        }
    }
    public void Dmg_lvl_3()
    {
        if (Pay())
        {
            playerStats.damageMultiplier += 0.03f;
            Next_dmg_lvl();
        }
    }
    public void Dmg_lvl_4()
    {
        if (Pay())
        {
            playerStats.damageMultiplier += 0.05f;
            Next_dmg_lvl();
        }
    }
    public void Dmg_lvl_5()
    {
        if (Pay())
        {
            playerStats.damageMultiplier += 0.010f;
            Next_dmg_lvl();
        }
    }
    public void Hp_lvl_1()
    {
        if (Pay())
        {
            playerStats.max_health += 1;
            ui_stats.Set_new_max_hp(1);
            ui_stats.Add_hp(1f);
            Next_hp_lvl();
        }
    }
    public void Hp_lvl_2()
    {
        if (Pay())
        {
            playerStats.max_health += 1;
            ui_stats.Set_new_max_hp(1);
            ui_stats.Add_hp(1f);
            Next_hp_lvl();
        }
    }
    public void Hp_lvl_3()
    {
        if (Pay())
        {
            playerStats.max_health += 3;
            ui_stats.Set_new_max_hp(3);
            ui_stats.Add_hp(3f);
            Next_hp_lvl();
        }
    }
    public void Hp_lvl_4()
    {
        if (Pay())
        {
            playerStats.max_health += 5;
            ui_stats.Set_new_max_hp(5);
            ui_stats.Add_hp(5f);
            Next_hp_lvl();
        }
    }
    public void Hp_lvl_5()
    {
        if (Pay())
        {
            playerStats.max_health += 10;
            ui_stats.Set_new_max_hp(10);
            ui_stats.Add_hp(10f);
            Next_hp_lvl();
        }
    }

    public void Func()
    {
        switch (skill_case)
        {
            case 0:
                Dmg_lvl_1();
                break;
            case 1:
                Dmg_lvl_2();
                break;
            case 2:
                Dmg_lvl_3();
                break;
            case 3:
                Dmg_lvl_4();
                break;
            case 4:
                Dmg_lvl_5();
                break;
            case 5:
                Hp_lvl_1();
                break;
            case 6:
                Hp_lvl_2();
                break;
            case 7:
                Hp_lvl_3();
                break;
            case 8:
                Hp_lvl_4();
                break;
            case 9:
                Hp_lvl_5();
                break;
        }
    }

    public void Exit()
    {
        accept.SetActive(false);
    }

}
