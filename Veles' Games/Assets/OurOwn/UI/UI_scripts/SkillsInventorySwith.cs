using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillsInventorySwith : MonoBehaviour
{
    bool _in_inventory = true;
    public GameObject inventory;
    public GameObject skills;

    public GameObject in_button;
    public GameObject sk_button;

    public void Swith()
    {
        _in_inventory = !_in_inventory;
        inventory.SetActive(!inventory.activeInHierarchy);
        skills.SetActive(!skills.activeInHierarchy);
        in_button.GetComponent<Button>().enabled = !inventory.activeInHierarchy;
        sk_button.GetComponent<Button>().enabled = !skills.activeInHierarchy;
    }
}
