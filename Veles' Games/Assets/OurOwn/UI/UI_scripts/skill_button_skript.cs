using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class skill_button_skript : MonoBehaviour
{
    public int skill_case = 0;
    public GameObject accept;
    public TextMeshProUGUI skill_name;
    public TextMeshProUGUI skill_cost;
    public TextMeshProUGUI self_skill_name;
    public int self_skill_cost;
    public Skills_manager skills_;

    public void ShowAccept()
    {
        accept.SetActive(true);
        skills_.skill_case = skill_case;
        skills_.skill_cost = self_skill_cost;
        skill_name.text = self_skill_name.text;
        skill_cost.text = self_skill_cost.ToString();
    }
}
