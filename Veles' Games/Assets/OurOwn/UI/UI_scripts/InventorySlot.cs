using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour, IDropHandler
{
    public Image image;
    public Color selectedColor, notSelectedColor;
    public ThirdPersonController playerController;

    private void Start()
    {
        playerController = ThirdPersonController.instance;
        Deselect();
    }

    public void Select()
    {
        image.color = selectedColor;
    }

    public void Deselect()
    {
        image.color = notSelectedColor;
    }

    public virtual void OnDrop(PointerEventData eventData)
    {
        if (transform.childCount == 0)
        {
            InventoryItem inventoryItem = eventData.pointerDrag.GetComponent<InventoryItem>();
            if (inventoryItem.parentAfterDrag.TryGetComponent<WeaponSlot>(out WeaponSlot weaponSlot))
            {
                weaponSlot.equipmentSystem.UneqipWeapon();
            }
            inventoryItem.parentAfterDrag = transform;
        }
        else
        {
            InventoryItem itemHere = transform.GetChild(0).GetComponent<InventoryItem>();
            InventoryItem itemFrom = eventData.pointerDrag.GetComponent<InventoryItem>();
            if (itemHere.item == itemFrom.item)
            {
                if (itemHere.count + itemFrom.count <= itemHere.item.maxCount)
                {
                    itemHere.count += itemFrom.count;
                    Destroy(itemFrom.gameObject);
                    itemHere.RefreshCount();
                }
                else if (itemHere.count + itemFrom.count <= 2 * itemHere.item.maxCount)
                {
                    int toReplace = itemHere.item.maxCount - itemHere.count;
                    //Debug.Log("replacing " + toReplace);
                    itemHere.count = itemHere.item.maxCount;
                    itemFrom.count -= toReplace;
                    //Debug.Log("Now here is " + itemHere.count);
                    //Debug.Log("Now therre is " + itemFrom.count);
                    itemHere.RefreshCount();
                    itemFrom.RefreshCount();
                }
            }
            else 
            {
                
                if (itemFrom.parentAfterDrag.TryGetComponent<WeaponSlot>(out WeaponSlot weaponSlot) && itemHere.item.type == ItemType.Weapon && weaponSlot)
                {
                    weaponSlot.equipmentSystem.UneqipWeapon();
                    weaponSlot.equipmentSystem.EquipWeapon(itemHere.item);
                }

                itemHere.transform.SetParent(itemFrom.parentAfterDrag);
                itemFrom.parentAfterDrag = transform;
            }
        }
    }
}
