using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class InventoryManager : MonoBehaviour
{
    public static InventoryManager instance;

    public ItemBase[] startItems;

    public int maxStackedItems = 5;
    public InventorySlot[] inventorySlots;
    public RectTransform[] rectTransforms;
    public GameObject inventoryItemPrefab;
    public WeaponSlot weaponSlot;
    public InventorySlot buffSlot;

    int selectedSlot = -1;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        rectTransforms= new RectTransform[inventorySlots.Length];
        for (int i = 0; i < inventorySlots.Length; i++)
        {
            rectTransforms[i] = inventorySlots[i].GetComponent<RectTransform>();
        }

        foreach (var item in startItems)
        {
            AddItem(item);
        }
        ChangeSelectedSlot(0);
    }

    private void Update()
    {

        for (int i = 0; i < inventorySlots.Length; i++)
        {
            RectTransform rectTransform = rectTransforms[i];
            Vector2 localMouse = rectTransform.InverseTransformPoint(Mouse.current.position.ReadValue());
            if (rectTransform.rect.Contains(localMouse))
            {
                ChangeSelectedSlot(i);
                return;
            }
        }
    }

    void ChangeSelectedSlot(int newValue)
    {   
        if (selectedSlot >= 0)
            inventorySlots[selectedSlot].Deselect();

        inventorySlots[newValue].Select();
        selectedSlot = newValue;
    }


    public bool AddItem(ItemBase item)
    {
        // Check if any slot has the same item with count lower than max

        /*for (int i = 0; i < inventorySlots.Length; i++)
        {
            InventorySlot slot = inventorySlots[i];
            InventoryItem itemInSlot = slot.GetComponentInChildren<InventoryItem>();

            if (itemInSlot != null &&
                itemInSlot.item == item &&
                itemInSlot.count < maxStackedItems && 
                itemInSlot.item.stackable)
            {
                itemInSlot.count++;
                itemInSlot.RefreshCount();
                return true;
            }
        }*/

        // Find an empty slot

        for (int i = 0; i < inventorySlots.Length; i++)
        {
            InventorySlot slot = inventorySlots[i];
            InventoryItem itemInSlot = slot.GetComponentInChildren<InventoryItem>();
            InventoryWeaponItem weaponInSlot = slot.GetComponentInChildren<InventoryWeaponItem>();

            if (itemInSlot == null )
            {
                SpawnNewItem(item, slot);
                return true;
            }
        }

        return false;
    }

    void SpawnNewItem(ItemBase item, InventorySlot slot)
    {
        GameObject newItemGo = Instantiate(inventoryItemPrefab, slot.transform);
        InventoryItem inventoryItem = newItemGo.GetComponent<InventoryItem>();
        inventoryItem.InitiallizeItem(item);
    }


    public ItemBase GetSelectedItem(bool use)
    {
        InventorySlot slot = inventorySlots[selectedSlot];
        InventoryItem itemInSlot = slot.GetComponentInChildren<InventoryItem>();
        if (itemInSlot != null)
        {
            ItemBase item = itemInSlot.item;

            if (use)
            {
                itemInSlot.count--;
                if (itemInSlot.count <= 0)
                {
                    Destroy(itemInSlot.gameObject);
                }
                else
                {
                    itemInSlot.RefreshCount();
                }
            }

            return item;
        }
        return null;
    }
}
