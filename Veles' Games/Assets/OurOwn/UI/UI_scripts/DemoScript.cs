using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoScript : MonoBehaviour
{
    public InventoryManager inventoryManager;
    public ItemBase[] itemsToPickup;

    public void PickupItem(int id)
    {
        bool result = inventoryManager.AddItem(itemsToPickup[id]);
        /*if (result)
        {
            Debug.Log("Item added");
        }
        else
        {
            Debug.Log("Item Not Added");
        }*/
    }

    public void GetSelectedItem()
    {
        ItemBase receivedItem = inventoryManager.GetSelectedItem(false);
        /*if (receivedItem != null)
        {
            Debug.Log("Received item " + receivedItem);
        }
        else
        {
            Debug.Log("Recieved no Item");
        }*/
    }
}
