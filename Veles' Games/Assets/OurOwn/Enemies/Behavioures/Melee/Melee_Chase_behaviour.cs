using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Melee_Chase_behaviour : StateMachineBehaviour
{
    NavMeshAgent agent;
    Transform player;
    Enemy_stats enemy_stats;
    GameObject weapon;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy_stats = animator.gameObject.GetComponent<Enemy_stats>();
        agent = animator.GetComponent<NavMeshAgent>();
        agent.speed = enemy_stats.chasinging_speed;
        agent.angularSpeed = enemy_stats.rotation_speed;
        player = GameObject.Find("Player").transform;
        //weapon = GameObject.Find("DamageCollider");
        //weapon.GetComponent<BoxCollider>().enabled = false;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        agent.SetDestination(player.position);
        float distance = Vector3.Distance(animator.transform.position, player.position);
        float spawn_distance = Vector3.Distance(animator.transform.position, enemy_stats.point_of_patrol.position);


        if (distance <= enemy_stats.attacking_range)animator.SetBool("is_attacking", true);

        if (distance >= enemy_stats.player_chase_dist || spawn_distance >= enemy_stats.chase_area_radius)animator.SetBool("Is_chasing", false);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        agent.SetDestination(agent.transform.position);
    }

}
