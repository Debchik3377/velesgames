using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee_Attack_behaviour : StateMachineBehaviour
{
    Transform player;
    Enemy_stats enemy_stats;
    float timer;

    private int _attack_delay;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy_stats = animator.gameObject.GetComponent<Enemy_stats>();
        timer = 0;
        player = enemy_stats.player.transform;
        _attack_delay = Animator.StringToHash("attack_delay");
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.transform.LookAt(player.position);

        if (timer > stateInfo.length) animator.SetBool(_attack_delay, true);

        timer += Time.deltaTime;
    }
}
