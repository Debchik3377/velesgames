using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Patrol_behaviour : StateMachineBehaviour
{
    NavMeshAgent agent;
    int next_point = 0;
    GameObject player;
    Enemy_stats enemy_stats;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy_stats = animator.gameObject.GetComponent<Enemy_stats>();
        player = GameObject.Find("Player");
        agent = animator.GetComponent<NavMeshAgent>();

        agent.speed = enemy_stats.patroling_speed;
        agent.angularSpeed = enemy_stats.rotation_speed;
        agent.SetDestination(enemy_stats.patrol.transform.GetChild(next_point).position);
        enemy_stats.point_of_patrol = enemy_stats.patrol.transform.GetChild(next_point);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //����������� �� ������
        if (agent.remainingDistance <= agent.stoppingDistance)
        {
            next_point += 1;
            if (next_point >= enemy_stats.patrol.transform.childCount) next_point = 0;
            agent.SetDestination(enemy_stats.patrol.transform.GetChild(next_point).position);
            enemy_stats.point_of_patrol = enemy_stats.patrol.transform.GetChild(next_point);
            animator.SetBool("Is_patroling", false);
        }

        //�������� ���� ������, ����������� � ���������� ����� ������ � �������

        Vector3 target_direction = player.transform.position - animator.transform.position;
        float distance = Vector3.Distance(animator.transform.position, player.transform.position);
        float view_angle = Vector3.Angle(target_direction, animator.transform.forward);

        GameObject playerEyes = player.transform.GetChild(5).gameObject;
        GameObject enemyEyes = animator.transform.GetChild(2).gameObject;
        bool can_see = !(Physics.Linecast(enemyEyes.transform.position, playerEyes.transform.position));

        if (enemy_stats.is_distance && distance <= enemy_stats.keep_distance && enemy_stats.is_distance) animator.SetBool("is_retreating", true);

        if ((distance < enemy_stats.detection_range && view_angle <= enemy_stats.view_angle && can_see) || distance <= enemy_stats.hearing_radius)animator.SetBool("Is_chasing", true);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        agent.SetDestination(agent.transform.position);
    }

}
