using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Distance_retreat : StateMachineBehaviour
{
    NavMeshAgent agent;
    Transform player;
    Enemy_stats enemy_stats;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy_stats = animator.gameObject.GetComponent<Enemy_stats>();
        player = GameObject.Find("Player").transform;
        agent = animator.GetComponent<NavMeshAgent>();
        agent.speed = enemy_stats.retreating_speed;

        if (!animator.GetBool("retreat_cooldown"))
        {
            Vector3 v = agent.transform.position - player.position;
            agent.SetDestination(agent.transform.position + v.normalized * enemy_stats.retreating_distance);
        }

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        if (agent.remainingDistance <= agent.stoppingDistance)
        {
            agent.SetDestination(agent.transform.position);
            animator.SetBool("retreat_cooldown", true);
            enemy_stats.Cooldown(animator);
            animator.SetBool("is_retreating", false);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
