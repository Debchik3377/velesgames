using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Distance_Attack_behaviour : StateMachineBehaviour
{
    Transform player;
    Enemy_stats enemy_stats;
    float timer;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = GameObject.Find("Player").transform;
        enemy_stats = animator.gameObject.GetComponent<Enemy_stats>();
        timer = 0;
        animator.SetBool("is_shooting", true);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.transform.LookAt(player.position);

        if (timer > stateInfo.length) animator.SetBool("attack_delay", true);

        timer += Time.deltaTime;
    }
}
