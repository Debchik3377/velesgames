using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Idle_behaviour : StateMachineBehaviour
{
    Transform player;
    Enemy_stats enemy_stats;
    float timer = 0;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy_stats = animator.gameObject.GetComponent<Enemy_stats>();
        player = GameObject.Find("Player").transform;
        timer = 0;
        enemy_stats.point_of_patrol = enemy_stats.patrol.transform.GetChild(0);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //����������� ��� ���
        if (enemy_stats.will_patrol == true && timer >= enemy_stats.stopping_time) animator.SetBool("Is_patroling", true);
        timer += Time.deltaTime;

        Vector3 target_direction = player.transform.position - animator.transform.position;
        float distance = Vector3.Distance(animator.transform.position, player.transform.position);
        float view_angle = Vector3.Angle(target_direction, animator.transform.forward);

        GameObject playerEyes = player.GetChild(5).gameObject;
        GameObject enemyEyes = animator.transform.GetChild(2).gameObject;
        bool can_see = !(Physics.Linecast(enemyEyes.transform.position, playerEyes.transform.position));

        if (distance <= enemy_stats.keep_distance && enemy_stats.is_distance) animator.SetBool("is_retreating", true);

        if ((distance < enemy_stats.detection_range && view_angle <= enemy_stats.view_angle && can_see) || distance <= enemy_stats.hearing_radius) animator.SetBool("Is_chasing", true);
    }

}
