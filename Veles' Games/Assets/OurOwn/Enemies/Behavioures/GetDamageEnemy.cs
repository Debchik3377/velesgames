using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SA
{
    public class GetDamageEnemy : MonoBehaviour
    {
        Enemy_stats enemy_stats;
        private void OnTriggerEnter(Collider other)
        {
            enemy_stats = gameObject.GetComponent<Enemy_stats>();
            if (other.gameObject.tag == "Player" && other.gameObject.layer == 7)
            {
                enemy_stats.hp -= other.GetComponent<DamageDealer>().weaponDamage; //���������� ���� ���� � ������
            }
        }
    }
}

