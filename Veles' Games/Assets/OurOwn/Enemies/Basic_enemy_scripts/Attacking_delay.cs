using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacking_delay : StateMachineBehaviour
{
    Transform player;
    Enemy_stats enemy_stats;
    float timer;

    private int _attack;
    private int _is_attacking;
    private int _attack_delay;
    private int _is_retreating;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy_stats = animator.gameObject.GetComponent<Enemy_stats>();
        if (enemy_stats.is_distance)_is_retreating = Animator.StringToHash("is_retreating");
        AssignAnimatorIDs();
        timer = 0;
        player = enemy_stats.player.transform;
        int r = Random.Range(0, enemy_stats.attacks_count);
        animator.SetInteger(_attack, r);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        float distance = Vector3.Distance(animator.transform.position, player.position);
        if (distance > enemy_stats.attacking_range && animator.GetBool(_attack_delay))
        {
            animator.SetBool(_is_attacking, false);
            animator.SetBool(_attack_delay, false);
        }

        animator.transform.LookAt(player.position);

        if (enemy_stats.is_distance && distance <= enemy_stats.keep_distance) animator.SetBool(_is_retreating, true);

        if (timer >= enemy_stats.attacking_delay)
        {
            animator.SetBool(_attack_delay, false);
        }
        timer += Time.deltaTime;
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    private void AssignAnimatorIDs()
    {
        //_is_retreating = Animator.StringToHash("is_retreating");
        _attack = Animator.StringToHash("attack");
        _is_attacking = Animator.StringToHash("is_attacking");
        _attack_delay = Animator.StringToHash("attack_delay");

    }

}
