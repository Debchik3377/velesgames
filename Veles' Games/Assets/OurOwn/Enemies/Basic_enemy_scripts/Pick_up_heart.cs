using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pick_up_heart : MonoBehaviour
{
    public UI_lean_tween statsManager;

    private void Awake()
    {
        statsManager = GameObject.Find("UIStatsManager").GetComponent<UI_lean_tween>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Destroy(gameObject);
            statsManager.Add_hearts(); 
        }
    }
}
