using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class can_shoot : StateMachineBehaviour
{
    Transform player;
    [SerializeField] LayerMask layerMask;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = GameObject.Find("Player").transform;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        bool can_shot = !Physics.Linecast(animator.transform.GetChild(7).transform.position, player.transform.GetChild(6).transform.position, layerMask);//true = ��� �����������

        animator.SetBool("can_shoot", can_shot);

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

}
