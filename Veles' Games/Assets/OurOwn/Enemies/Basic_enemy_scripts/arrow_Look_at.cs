using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrow_Look_at : MonoBehaviour
{
    public GameObject target;

    private void Start()
    {
        target = GameObject.Find("Target");
    }

    void Update()
    {
        transform.LookAt(target.transform.position);
    }
}
