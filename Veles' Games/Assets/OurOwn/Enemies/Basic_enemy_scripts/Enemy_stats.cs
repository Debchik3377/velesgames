using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_stats : MonoBehaviour
{
    [Header("������� ����������")]
    public float hp = 100;
    public float damage = 5;
    public GameObject prefab;

    [Header("��������� �������")]
    public bool will_patrol = true; //����������� ��� ���
    public float patroling_speed = 2; //�������� �� ����� �������
    public float view_angle = 45; //���� ������
    public float detection_range = 50; //��������� ������
    public float hearing_radius = 3; //���\���� �����, ������ �� ���� ���� �� ������
    public GameObject patrol; //���� ������� �������, ������� ���������� ����

    [Header("��������� �� ������")]
    public float stopping_time = 5; //�� ������� ����������� �� ������ �����

    [Header("��������� ������")]
    public float chasinging_speed = 4; //�������� ���� �� �������
    public float player_chase_dist = 100; //���� ����� ���������� ������ ����� �����, ���� ����� �����
    public float chase_area_radius = 100; //���� �� ����� ������� �� ����� �������������� ������ ����� �����

    [Header("��������� �����")]
    public float attacking_range = 2; //������ �����
    public float attacking_delay = 3; //�������� ����� ����� ����������(����� ����� ��������, ������� atacking_delay ������ � ����� ������� �����)
    public int attacks_count = 1; //���-�� �������������� ����
    public GameObject weapon; //������ ���� �������!!! ��� �������? �? � ��������� ���? ������� ���� ������!!


    [Header("��������� ��� ���������")]
    public bool is_distance = false; //������ ���� �������, ��� ��� �����������
    public float keep_distance = 50; //�� ������� ������ ��������� ��������� �� ������, ���� ������(
    public float retreating_speed = 5; //�������� �����������
    public float retreat_cooldown = 5; //��������� ����� �����������
    public float retreating_distance = 20; //�������� �� ��� ���-�� ������, � ����� ����������� �� retreat_cooldown ������
    public GameObject projectile; //��� �������
    public Transform shooting_position; //������ �������
    public float shooting_power = 20; //���� ��������
    public Transform projectile_in_hand; //������� ������� ��� ��������

    [Header("�������������� ���������")]
    [Header("������������� �������� �� ���������)")]
    public float rotation_speed = 10000; //�������� ��������, ��� ����, �� ���� �����, ������ ���������
    public Transform point_of_patrol; //��� ����� ��� ������ ��������, �� �� ������, �����!!!
    Animator animator;
    public GameObject player;
    public GameObject heart;
    public Transform heart_position;

    private int _animIDDamage; // ������� �����
    private int _is_shooting; // ������� �� �����
    private int _retreat_cooldown; // ������� �� �����


    //���� �������� ��� ���� �������

    IEnumerator ExecuteAfterTime(Animator animator)
    {
        yield return new WaitForSeconds(retreat_cooldown);
        animator.SetBool(_retreat_cooldown, false);
    }
    public void Cooldown(Animator animator)
    {
        StartCoroutine(ExecuteAfterTime(animator));
    }
    private void Start()
    {
        animator = GetComponent<Animator>();
        //Physics.IgnoreCollision(player.GetComponent<Collider>(), GetComponent<Collider>());
        AssignAnimatorIDs();
    }
    public void Shot()
    {
        GameObject new_projectile = Instantiate(projectile, shooting_position.position, shooting_position.rotation);
        new_projectile.GetComponent<Rigidbody>().velocity = new_projectile.transform.forward * shooting_power;
        //new_projectile.GetComponent<ArrowMove>
        //new_projectile.GetComponent<Rigidbody>().AddForce(new_projectile.transform.forward * shooting_power);
        animator.SetBool(_is_shooting, false);
        projectile_in_hand.gameObject.SetActive(false);
        new_projectile.GetComponent<arrow_Look_at>().enabled = false;
    }

    public void Death()
    {
        GameObject new_projectile = Instantiate(heart, heart_position.position, heart_position.rotation);
        Destroy(gameObject);
    }

    private void Update()
    {

        if (is_distance)
        {
            if (animator.GetBool(_is_shooting))
            {
                projectile_in_hand.gameObject.SetActive(true);
            }

        }
    }
    // For Damage
    public List<DamageDealer> weaponDealers;
    public void TakeDamage(float damageAmount)
    {
        hp -= damageAmount;
        animator.SetTrigger(_animIDDamage);

        if (hp <= 0) Death();
    }

    public void StartDealDamage(int i = 0)
    {
        Debug.Log("Start");
        weaponDealers[i].StartDealDamage();
    }

    public void EndDealDamage(int i = 0)
    {
        weaponDealers[i].EndDealDamage();
    }

    private void AssignAnimatorIDs()
    {
        _animIDDamage = Animator.StringToHash("damage");
        _is_shooting = Animator.StringToHash("is_shooting");
        _retreat_cooldown = Animator.StringToHash("retreat_cooldown");
    }
}
