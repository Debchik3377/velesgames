using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowDamage : MonoBehaviour
{
    private bool canDealDamage = true;

    [SerializeField] private float arrowDamage;
    [SerializeField] private LayerMask layerMask;
    private bool isHit;
    private RaycastHit hit;
    private Ray ray;

    // Start is called before the first frame update
    void Start()
    {
        canDealDamage = false;
    }

    // Update is called once per frame
    void Update()
    {
        /*isHit = false;
        ray = new Ray(transform.position, transform.forward * 5);
        if (canDealDamage && Physics.Raycast(transform.position, transform.forward, out hit, gameObject.transform.localScale.z * 10f, layerMask))
        {
            GameObject player = hit.collider.gameObject;
            if (player.name == "Player")
            {
                isHit = true;
                player.GetComponent<PlayerStatistics>().TakeDamage(arrowDamage);
                canDealDamage = false;
            }
        }*/
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerStatistics player = other.GetComponent<PlayerStatistics>();
        if (player)
        {
            isHit = true;
            Debug.Log("Trigger");
            player.TakeDamage(arrowDamage);
            canDealDamage = false;
        }
    }

    private void OnDrawGizmos()
    {
        if (isHit)
            Gizmos.color = Color.green;
        else
            Gizmos.color = Color.red;

        Gizmos.DrawRay(ray);

    }
}
