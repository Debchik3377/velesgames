using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "EnemyManager")]
public class EnemyManager : ScriptableObject
{
    public List<Vector3> enemies;
    public List<GameObject> patrols;
    public List<GameObject> prefabs;

    public void Save()
    {
        enemies.Clear();
        patrols.Clear();
        prefabs.Clear();
        GameObject _enemies = GameObject.Find("Enemies");

        for (int i = 0; i < _enemies.transform.childCount; i++)
            enemies.Add(_enemies.transform.GetChild(i).gameObject.transform.position);

        for (int i = 0; i < _enemies.transform.childCount; i++)
            patrols.Add(_enemies.transform.GetChild(i).gameObject.GetComponent<Enemy_stats>().patrol);

        for (int i = 0; i < _enemies.transform.childCount; i++)
            prefabs.Add(_enemies.transform.GetChild(i).gameObject.GetComponent<Enemy_stats>().prefab);

    }

    public void Load()
    {
        GameObject _enemies = GameObject.Find("Enemies");

        int n = _enemies.transform.childCount;

        if (_enemies != null)
        {
            for (int i = 0; i < n; i++)
            {
                Destroy(_enemies.transform.GetChild(i).gameObject);
            }

            for (int i = 0; i < n; i++)
            {
                GameObject enemy = Instantiate(prefabs[i], enemies[i], Quaternion.identity, parent: _enemies.transform);
                enemy.GetComponent<Enemy_stats>().patrol = patrols[i];
                enemy.GetComponent<Enemy_stats>().enabled = true;
                enemy.GetComponent<Animator>().enabled = true;
                enemy.GetComponent<NavMeshAgent>().enabled = true;
            }
        }
    }
}
