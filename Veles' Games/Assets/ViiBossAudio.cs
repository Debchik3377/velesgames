using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViiBossAudio : MonoBehaviour
{
    public void Roar1()
    {
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(Audiomanager.instance.ViiRoar1, transform);
        Audiomanager.instance.PlayViiRoar1();
    }
    public void Roar2()
    {
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(Audiomanager.instance.ViiRoar2, transform);
        Audiomanager.instance.PlayViiRoar2();
    }
    public void ViiFootsteps()
    {
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(Audiomanager.instance.ViiFootsteps, transform);
        Audiomanager.instance.PlayViiFootsteps();
    }
    public void ViiAgony()
    {
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(Audiomanager.instance.ViiAgony, transform);
        Audiomanager.instance.PlayViiAgony();
    }
    public void ViiAttack()
    {
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(Audiomanager.instance.ViiAgony, transform);
        Audiomanager.instance.PlayViiAttack();
    }
    public void ViiLand()
    {
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(Audiomanager.instance.ViiLand, transform);
        Audiomanager.instance.PlayViiLand();
    }
    public void ViiGetHit()
    {
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(Audiomanager.instance.ViiGetHit, transform);
        Audiomanager.instance.PlayViiGetHit();
    }
    public void ViiAttackGround()
    {
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(Audiomanager.instance.ViiAttackGround, transform);
        Audiomanager.instance.PlayViiAttackGround();
    }
}
