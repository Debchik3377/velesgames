using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FirstStage : StateMachineBehaviour
{
    public float curTime = 0f;
    public float waitTime = 3f;
    private bool setTime = false;

    public Animator animator;
    public UnityEngine.AI.NavMeshAgent agent;

    public Transform newPos;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GameObject boss = GameObject.Find("Boss");
        agent = boss.GetComponent<NavMeshAgent>();

        curTime = 0f;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        curTime += Time.deltaTime;
        if (curTime > waitTime && !setTime)
        {
            setTime = true;
            BossMono.instance.StartCycle();
        }

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //agent.destination = GameObject.Find("Player").transform.position;
        setTime = false;
       // Debug.Log(GameObject.Find("Player").transform.position);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
