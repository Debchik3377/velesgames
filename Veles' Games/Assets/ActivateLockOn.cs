using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateLockOn : MonoBehaviour
{
    [SerializeField] private Transform lockonTarget;
    private void OnTriggerEnter(Collider other)
    {
        CinemachineController player = other.GetComponent<CinemachineController>();
        if (player)
        {
            player.SetLockTarget(lockonTarget);
            player.TurnOnLockon();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        CinemachineController player = other.GetComponent<CinemachineController>();
        if (player)
        {
            player.SetLockTarget(other.transform);
            player.TurnOffLockon();
        }
    }

}
