using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BossHitSimple : StateMachineBehaviour
{
    NavMeshAgent Agent;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Agent = BossMono.instance.Agent;
        Agent.velocity = Vector3.zero;
        Agent.updatePosition = false;
        //agent.updateRotation = true;
        BossMono.instance.ResetAnimHit();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Agent.isOnNavMesh && Vector3.Distance(animator.transform.position, BossMono.instance.Player.transform.position) >= Agent.stoppingDistance + 1.5f)
        {
            animator.transform.LookAt(BossMono.instance.Player.transform.position);
        }

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //agent.updateRotation = true;
        Agent.updatePosition = true;
        BossMono.instance.ResetAnimHit();
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
