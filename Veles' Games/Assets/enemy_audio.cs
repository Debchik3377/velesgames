using FMOD.Studio;
using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy_audio : MonoBehaviour
{
    public enemy_terrain_checker tc;

    public EventReference _footsteps;
    public EventInstance footsteps;

    public EventReference _hit;
    public EventInstance hit;

    public EventReference _attack;
    public EventInstance attack;

    private void Start()
    {
        footsteps = Audiomanager.instance.CreateInstance(_footsteps);
        hit = Audiomanager.instance.CreateInstance(_hit);
        attack = Audiomanager.instance.CreateInstance(_attack);
        tc = GetComponent<enemy_terrain_checker>();
    }
    // Update is called once per frame
    void Update()
    {
        tc.ChangeFootsteps(transform.position);
    }

    public void Footsteps() { if (footsteps.isValid())
        {
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(footsteps, transform);
            footsteps.start();
        }
    }
    public void Hit()
    {
        if (hit.isValid())
        {
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(hit, transform);
            hit.start();
        }
    }
    public void Attack()
    {
        if (footsteps.isValid())
        {
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(attack, transform);
            attack.start();
        }
    }
}
