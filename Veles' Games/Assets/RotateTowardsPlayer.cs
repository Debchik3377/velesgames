using StarterAssets;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class RotateTowardsPlayer : StateMachineBehaviour
{
    public NavMeshTriangulation Triangulation;

    public BossMono bossScript;
    public Animator animator;
    public UnityEngine.AI.NavMeshAgent Agent;
    private ThirdPersonController player;

    public float rotation = 0f;
    public float stateSpeed = 5f;
    public float stateAcceleration = 5f;
    public float maxSpeed = 5f;
    [Header("How many hit animations after this movement")]
    public int hitCount = 0;

    // Animations
    public static int _animIDVelocity;
    public static int _animIDHit;




    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
        player = ThirdPersonController.instance;
        BossMono.instance.canRotate = true;
        bossScript = animator.transform.GetComponent<BossMono>();
        bossScript.ResetAnimHit();
        GameObject boss = GameObject.Find("Boss");
        Agent = boss.GetComponent<NavMeshAgent>();
        Agent.isStopped = false;
        Agent.updateRotation = false;

        Agent.speed = stateSpeed;
        Agent.acceleration = stateAcceleration;

        bossScript.GoTowardsPlayer();

        BossMono.instance.SetHitCount(hitCount);
        _animIDVelocity = Animator.StringToHash("Velocity");
        _animIDHit = Animator.StringToHash("Hit");
    }

    

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Vector3.Distance(animator.transform.position, BossMono.instance.Player.transform.position) >= Agent.stoppingDistance)
        {
            animator.transform.LookAt(player.transform.position);
            //Debug.Log(Agent.stoppingDistance + " - " + Agent.remainingDistance);
        }
        else if (Vector3.Distance(animator.transform.position, BossMono.instance.Player.transform.position) < Agent.stoppingDistance)
        {
            bossScript.StartHit();
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        bossScript.StopMoving();
        BossMono.instance.canRotate = true;
        Agent.isStopped = true; 
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
