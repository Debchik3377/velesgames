using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TypeSlot : InventorySlot, IDropHandler
{
    [SerializeField]
    private ItemType[] availiableTypes;
    public EquipmentSystem equipmentSystem;

    private void Start()
    {
        equipmentSystem = ThirdPersonController.instance.equipmentSystem;
        playerController = ThirdPersonController.instance;
    }

    public override void OnDrop(PointerEventData eventData)
    {
        Debug.Log(eventData.pointerDrag.gameObject);
        InventoryItem dragInventoryItem = eventData.pointerDrag.GetComponent<InventoryItem>();
        if (dragInventoryItem.item.type == ItemType.Potion)
        {
            PotionEffect potion = dragInventoryItem.item.Model.GetComponent<PotionEffect>();
            Debug.Log($"Potion {potion}");
            if (transform.childCount == 0)
            {
                dragInventoryItem.parentAfterDrag = transform;
                if (potion != null)
                {
                    playerController.equipmentSystem.currentBuff = dragInventoryItem.item;
                    playerController.BuffItem = dragInventoryItem;
                }
                
                
            }
            else
            {
                transform.GetChild(0).SetParent(dragInventoryItem.parentAfterDrag);
                dragInventoryItem.parentAfterDrag = transform;
                if (potion != null)
                {
                    playerController.equipmentSystem.currentBuff = dragInventoryItem.item;
                    playerController.BuffItem = dragInventoryItem;
                }
                
            }

        }
    }
}
