using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BossMono : MonoBehaviour
{
    public static BossMono instance;

    public UnityEngine.AI.NavMeshTriangulation Triangulation;
    public Animator animator;
    public GameObject Player;  // for custom editor drawing stuff
    public ThirdPersonController playerScript;  // for custom editor drawing stuff
    public UnityEngine.AI.NavMeshAgent Agent;
    [SerializeField]
    [Range(0f, 3f)]
    private float WaitDelay = 1f;
    [SerializeField]
    public bool UseMovementPrediction;
    [SerializeField]
    [Range(-1, 1)]
    public float MovementPredictionThreshold = 0;
    [SerializeField]
    [Range(0.25f, 2f)]
    public float MovementPredictionTime = 1f;
    public bool UseDistanceBasedPrediction;

    //Damagers
    [SerializeField] private DamageDealer[] damageColliders;
    public Transform RaycastPosition;

    // Stats
    public float HP;
    public int hitNum = 0;
    public int reactHitNum = 0;
    public bool immortable = false;
    public bool moveOnlyForward = false;

    // Stages
    public bool secondStage = false;
    public bool thirdStage = false;
    public bool dead = false;
    public UnityEvent death;



    public static int _animIDVelocity;
    public static int _animIDHit;
    public static int _animIDStartCycle;
    public static int _animIDSecondStage;
    public static int _animIDThirdStage;
    public static int _animIDReactToHit;
    public static int _animIDAgony;
    private int _animIDKnocked;

    private void Awake()
    {
        HP = 200f;
        if (instance == null)
            instance = this;

        Player = GameObject.Find("Player");
        playerScript = Player.GetComponent<ThirdPersonController>();
        Agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        animator = GetComponent<Animator>();

        _animIDVelocity = Animator.StringToHash("Velocity");
        _animIDHit = Animator.StringToHash("Hit");
        _animIDStartCycle = Animator.StringToHash("StartCycle");
        _animIDSecondStage = Animator.StringToHash("SecondStage");
        _animIDThirdStage = Animator.StringToHash("ThirdStage");
        _animIDReactToHit = Animator.StringToHash("ReactHit");
        _animIDAgony = Animator.StringToHash("Agony");
        _animIDKnocked= Animator.StringToHash("Knocked");

        animator.SetInteger(_animIDReactToHit, -1);
        controller = Player.transform.GetComponent<CinemachineController>();
    }

    // JUMP
    [SerializeField] private float stepsRadius = 0f;
    [SerializeField] private float stepShakeIntensity = 0f;
    [SerializeField] private float stepShakeTime = 0f;
    [SerializeField] private CinemachineController controller;
    public void ShakeSteps()
    {
        float dist = Vector3.Distance(transform.position, Player.transform.position);
        if (dist < stepsRadius)
        {
            // ����� ����� ������� ���������
            float intencity = stepShakeIntensity * (dist / stepsRadius);
            controller.ShakeCam(intencity, stepShakeTime);
        }
    }

    [SerializeField] private float jumpRadius = 0f;
    [SerializeField] private float jumpShakeIntensity = 0f;
    [SerializeField] private float jumpShakeTime = 0f;
    public void ShakeJump()
    {
        float dist = Vector3.Distance(transform.position, Player.transform.position);
        if (dist < jumpRadius)
        {
            // ����� ������ ���������
            float intencity = jumpShakeIntensity * (dist / jumpRadius);
            controller.ShakeCam(intencity, jumpShakeTime);
        }
    }



    // JUMP ATTACK
    [Header("For jump attack")]
    [SerializeField] private float radius;
    [SerializeField] public LayerMask playerMask;

    public void JumpHit()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius, playerMask);

        foreach(var collider in colliders)
        {
            CinemachineController cinemachineController = collider.GetComponent<CinemachineController>();
            if (cinemachineController)
            {
                ThirdPersonController.instance.inAction = true;
                collider.transform.GetComponent<Animator>().SetTrigger("Knocked");
            }
        }
    }

    public void EarthQuake()
    {

    }

    public void ActivateDamage()
    {
        Debug.Log("Activating");
        for (int i = 0; i < damageColliders.Length; i++)
        {
            damageColliders[i].StartDealDamage();
        }
    }

    public void DeactivateDamage()
    {
        Debug.Log("Deactivating");
        for (int i = 0; i < damageColliders.Length; i++)
        {
            damageColliders[i].EndDealDamage();
        }
    }

    public void ReactHit()
    {
        int ind = Random.Range(0, reactHitNum);
        animator.SetInteger(_animIDReactToHit, ind);
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(Audiomanager.instance.ViiGetHit, transform);
        Audiomanager.instance.PlayViiGetHit();

    }

    public void ResetReactHit()
    {
        animator.SetInteger(_animIDReactToHit, -1);
    }

    public void SetHitCount(int hitCount)
    {
        hitNum = hitCount;
    }

    public void StartHit()
    {
        Agent.isStopped = true;
        int hit = UnityEngine.Random.Range(0, hitNum);
        animator.SetInteger(_animIDHit, hit);
    }

    public void GetDamage(float damage)
    {
        HP -= damage;
        Debug.Log(HP);
        ReactHit();
        if (!secondStage && HP <= 100)
        {
            secondStage = true;
            StartSecondStage();
        }
        else if (!thirdStage && HP <= 50)
        {
            thirdStage = true;
            StartThirdStage();
        }
        else if (!dead && HP <= 0)
        {
            dead = true;
            StartAgony();
        }
    }

    public void GoToRandomPoint()
    {
        StartCoroutine(DoMoveToRandomPoint());
    }

    public void GoTowardsPlayer()
    {
        StartCoroutine(DoMoveToPlayer());
    }

    public void StopMoving()
    {
        Agent.isStopped = true;
        StopAllCoroutines();
    }

    public void ChangeMovingForward(bool moveForward)
    {
        moveOnlyForward = moveForward;
    }

    private IEnumerator DoMoveToRandomPoint()
    {
        //Agent.enabled = true;
        Agent.isStopped = false;
        WaitForSeconds Wait = new WaitForSeconds(WaitDelay);
        while (true)
        {
            int index = Random.Range(1, Triangulation.vertices.Length - 1);
            /*Agent.SetDestination(Vector3.Lerp(
                Triangulation.vertices[index],
                Triangulation.vertices[index + (Random.value > 0.5f ? -1 : 1)],
                Random.value)
            );*/

            Agent.destination = Vector3.Lerp(
                Triangulation.vertices[index],
                Triangulation.vertices[index + (Random.value > 0.5f ? -1 : 1)],
                Random.value);

            yield return null;
            yield return new WaitUntil(() => Agent.remainingDistance <= Agent.stoppingDistance);
            yield return Wait;
        }
    }

    private IEnumerator DoMoveToPlayer()
    {
        WaitForSeconds repathingDelay = new WaitForSeconds(0.3f);
        Agent.enabled = true;
        Agent.isStopped = false;
        while (true)
        {
            if (!UseMovementPrediction)
            {
                Agent.SetDestination(Player.transform.position);
            }
            else
            {
                float timeToPlayer = Vector3.Distance(Player.transform.position, transform.position) 
                    / Agent.speed;
                if (timeToPlayer > MovementPredictionTime)
                {
                    timeToPlayer = MovementPredictionTime;
                }

                Vector3 targetPosition = Player.transform.position + playerScript.AverageVelocity * timeToPlayer;
                Vector3 directionToTarget = (targetPosition - transform.position).normalized;
                Vector3 directionToPlayer = (Player.transform.position - transform.position).normalized;

                float dot = Vector3.Dot(directionToPlayer, directionToTarget);

                if (dot < MovementPredictionThreshold)
                {
                    targetPosition = Player.transform.position;
                }

                if (moveOnlyForward)
                {
                    targetPosition.x = 0;
                    targetPosition.y = 0;
                }
                Debug.Log(Agent + " " + Agent.isActiveAndEnabled);
                //Agent.SetDestination(targetPosition);
                Agent.destination = targetPosition;
            }

            yield return repathingDelay;
        }
    }

    public void ResetAnimHit()
    {
        int resetValue = -1;
        animator.SetInteger(_animIDHit, resetValue);
    }

    public void StartCycle()
    {
        animator.SetTrigger(_animIDStartCycle);
    }

    public void StartSecondStage()
    {
        animator.SetTrigger(_animIDSecondStage);
    }

    public void StartThirdStage()
    {
        animator.SetTrigger(_animIDThirdStage);
    }

    public void StartAgony()
    {
        animator.SetTrigger(_animIDAgony);
    }

    public bool canRotate = true;
    public void StopTurning()
    {
        canRotate = false;
        Debug.Log("Stopped turning");
        //Agent.updateRotation = false;
    }
}
